import com.diffplug.gradle.spotless.SpotlessExtension
import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.gradle.api.tasks.wrapper.Wrapper.DistributionType

buildscript {
  repositories { mavenCentral() }
  dependencies {
    classpath("com.diffplug.spotless:spotless-plugin-gradle:${Versions.spotlessGradle}")
  }
}

subprojects {
  apply<ApplicationPlugin>()
  apply<JavaBasePlugin>()
  apply<JacocoPlugin>()
  apply(plugin = "com.diffplug.spotless")

  repositories { mavenCentral() }

  dependencies {
    add("testImplementation", platform("org.junit:junit-bom:${Versions.junit}"))
    add("testImplementation", "org.junit.jupiter:junit-jupiter")
    add("testRuntimeOnly", "org.junit.platform:junit-platform-launcher")
  }

  configure<JavaPluginExtension> {
    sourceCompatibility = JavaVersion.VERSION_21
    targetCompatibility = JavaVersion.VERSION_21
  }

  configure<SpotlessExtension> {
    kotlin { ktfmt(Versions.ktfmt) }
    kotlinGradle { ktfmt(Versions.ktfmt) }
    java { palantirJavaFormat(Versions.palantirJavaFormat) }
  }
}

tasks {
  withType<AbstractArchiveTask> {
    isPreserveFileTimestamps = false
    isReproducibleFileOrder = true
  }
  withType<Test> {
    useJUnitPlatform()
    testLogging {
      exceptionFormat = TestExceptionFormat.FULL
      showExceptions = true
    }
  }
  wrapper {
    distributionType = DistributionType.ALL
    gradleVersion = Versions.gradle
  }
}

check(JavaVersion.current().isCompatibleWith(JavaVersion.VERSION_1_8)) {
  "Must be built with Java 8 or higher"
}
