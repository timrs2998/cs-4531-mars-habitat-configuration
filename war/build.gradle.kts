plugins { id("org.docstr.gwt.compiler") version Versions.gwtGradlePlugin }

dependencies {
  implementation(project(":jar"))

  implementation("com.allen-sauer.gwt.voices:gwt-voices:3.3.2")
  implementation("com.github.branflake2267:gwt-maps-api:3.10.0-alpha-7")
  implementation("com.google.gwt.google-apis:gwt-visualization:1.0.2")
}

gwt {
  minHeapSize = "512M"
  maxHeapSize = "1024M"

  gwtVersion = Versions.gwt
  modules("racoons.HabConfFrontend")
  src +=
      files(project(":jar").sourceSets.main.get().allJava.srcDirs) +
          files(project(":jar").sourceSets.main.get().output.resourcesDir)
}

// Fix error:
//   Reason: Task ':war:compileGwt' uses this output of task ':jar:processResources' without
// declaring an explicit or implicit dependency. This can lead to incorrect results being produced,
// depending on what order the tasks are executed.
tasks.getByName("compileGwt").dependsOn(project(":jar").tasks.getByName("processResources"))

tasks.register<Copy>("createPublic") {
  from(tasks.compileGwt.get().outputs, sourceSets.main.get().resources)
  into(rootDir.resolve("public/"))
}
