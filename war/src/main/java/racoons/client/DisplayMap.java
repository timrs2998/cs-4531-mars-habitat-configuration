package racoons.client;

import backend.common.Configuration;
import com.allen_sauer.gwt.voices.client.Sound;
import com.allen_sauer.gwt.voices.client.SoundController;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.ToggleButton;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * A class used for displaying a given configuration on a map.
 *
 * @author Tim
 */
public class DisplayMap extends Composite {

    // Variables for widgets
    private final transient VerticalPanel vp = new VerticalPanel();
    private final transient Label title = new Label("Configuration depicted on map");
    private final transient Button submit = new Button("Calculate Rover path");
    private final transient ToggleButton toggleLoggedModules = new ToggleButton("Toggle logged modules");

    // Variables for Map
    private final transient SimplePanel map_panel = new SimplePanel();
    private final transient MarsMap marsMap = new MarsMap(false);

    // The context variable
    private final Context context;
    private Configuration config;

    /**
     * Sole constructor.
     *
     * @param context
     */
    public DisplayMap(final Context context) {
        super();
        this.context = context;

        config = null;

        // Set styles for CSS
        title.addStyleName("mapHeader");
        submit.addStyleName("mapButton");

        // Button handler
        submit.addClickHandler(new SubmitHandler());
        toggleLoggedModules.addClickHandler(new ToggleHandler());

        map_panel.add(marsMap);

        // Add widgets to vertical panel
        vp.add(title);
        vp.add(submit);
        vp.add(toggleLoggedModules);
        vp.add(map_panel);

        // Housekeeping
        initWidget(vp);
        addStyleName("displayMap");
    }

    /**
     * Class used for handling clicks from submit button.
     */
    private class SubmitHandler implements ClickHandler {
        /**
         * Called when submit button is clicked.
         */
        @Override
        public void onClick(final ClickEvent event) {
            // Assume we have the modules to build this configuration
            assert context.core.getModules().size() >= config.getModules().size();

            submit.setEnabled(false);

            // Draw the paths and the configuration
            config = context.core.getBestLocation(config);
            drawConfigOnMap();
            marsMap.drawPath(context.core.getBestRoverPath(config));

            // Play sound file
            SoundController soundController = new SoundController();
            final Sound sound = soundController.createSound(Sound.MIME_TYPE_AUDIO_BASIC, "audio/MinPathCalculated.MP3");
            sound.play();
        }
    }

    private void drawConfigOnMap() {
        marsMap.clearModules();
        marsMap.clearOutlines();

        if (config != null && config.getModules().size() >= 1) {
            marsMap.drawConfiguration(config, MarsMap.getConfigurationOptions());
        }

        if (toggleLoggedModules.isDown() && context.core.getModules().size() >= 1) {
            marsMap.drawModuleList(context.core.getModules(), MarsMap.getLoggedOptions());
        }
    }

    private class ToggleHandler implements ClickHandler {
        @Override
        public void onClick(final ClickEvent event) {
            drawConfigOnMap();
        }
    }

    /**
     * Called when a user wants to change the configuration being displayed.
     * Draws the configuration on the map, and allows a minimum resource
     * solution to be calculated.
     *
     * @param config
     */
    public void setConfiguration(final Configuration config) {
        assert config != null;
        assert config.getModules().size() > 0;

        this.config = config;

        // Reenable the calculate button
        submit.setEnabled(true);

        // Draw the new configuration
        drawConfigOnMap();
    }
}
