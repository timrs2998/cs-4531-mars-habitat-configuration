package racoons.client;

import com.allen_sauer.gwt.voices.client.Sound;
import com.allen_sauer.gwt.voices.client.SoundController;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.URL;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONValue;
import com.google.gwt.jsonp.client.JsonpRequestBuilder;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * A class for showing the weather and providing the functionality to update it.
 *
 * @author James Walker
 */
public class Weather extends Composite {

    public Weather() {
        final VerticalPanel vp = new VerticalPanel();
        update = new Button("Update");
        text = new HTML("<div>No data to display</div>");
        vp.add(new Label("Weather Info:"));
        vp.add(new Label("Current Conditions:"));
        vp.add(text);
        vp.add(update);
        update.addStyleName("weatherButton");

        update.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                text.setHTML("<div>updating...</div>");
                update();
            }
        });

        initWidget(vp);
    }

    private final Button update;
    private final HTML text;

    private void update() {
        // Play sound file
        SoundController soundController = new SoundController();
        final Sound sound = soundController.createSound(Sound.MIME_TYPE_AUDIO_BASIC, "audio/Weather.MP3");
        sound.play();

        // Make a JSON request
        String url = "http://api.wunderground.com/api/743ae28eb7394598/" + "conditions/q/55812.json?";
        url = URL.encode(url);
        final JsonpRequestBuilder jsonp = new JsonpRequestBuilder();

        jsonp.setCallbackParam("callback");
        jsonp.requestObject(url, new AsyncCallback<JavaScriptObject>() {
            @Override
            public void onFailure(final Throwable caught) {
                Window.alert("JSONP onFailure");
            }

            @Override
            public void onSuccess(final JavaScriptObject s) {
                final JSONObject obj = new JSONObject(s);
                final String result = obj.toString();
                final JSONObject jA = (JSONObject) JSONParser.parseLenient(result);
                final JSONValue jObsv = jA.get("current_observation");
                final String sTry = jObsv.toString();
                final JSONObject jB = (JSONObject) JSONParser.parseLenient(sTry);
                final JSONValue jTemp = jB.get("temp_c");
                final JSONValue jVis = jB.get("visibility_km");
                final String temp = jTemp.toString();
                final String vis = jVis.toString();
                text.setHTML("<div>Temp: " + temp + " C" + "</br>" + "Visibility: " + vis + "km" + "</div>");
            }
        });
    }
}
