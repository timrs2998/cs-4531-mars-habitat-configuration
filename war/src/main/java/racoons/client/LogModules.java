package racoons.client;

import backend.common.Configuration;
import backend.common.Module;
import backend.common.Status;
import backend.common.Type;
import com.allen_sauer.gwt.voices.client.Sound;
import com.allen_sauer.gwt.voices.client.SoundController;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONNumber;
import com.google.gwt.json.client.JSONObject;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.storage.client.Storage;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.ListBox;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.visualization.client.AbstractDataTable;
import com.google.gwt.visualization.client.AbstractDataTable.ColumnType;
import com.google.gwt.visualization.client.DataTable;
import com.google.gwt.visualization.client.LegendPosition;
import com.google.gwt.visualization.client.VisualizationUtils;
import com.google.gwt.visualization.client.visualizations.ColumnChart;
import com.google.gwt.visualization.client.visualizations.ColumnChart.Options;

/**
 * @author Tim and James
 */
public class LogModules extends Composite {

    // Variables for widgets
    final VerticalPanel vp = new VerticalPanel();
    final HorizontalPanel download_panel = new HorizontalPanel();
    final Label download_text =
            new Label("Enter Case Number " + "to download(0 for default or 1-10, then click Download");
    final Button download_button = new Button("Download");
    final Button delete_button = new Button("Delete modules and configurations");
    final Label title = new Label("Log modules");
    final FlexTable table = new FlexTable();
    final Button submit = new Button("Log module");
    final ListBox listBox = new ListBox();

    // Variables for widgets contained in the table
    final Label type_label = new Label("Code number:");
    final Label orientation_label = new Label("Orientation:");
    final Label status_label = new Label("Status:");
    final Label location_label = new Label("Location:");

    final TextBox type_box = new TextBox();
    final ListBox orientation_box = new ListBox();
    final ListBox status_box = new ListBox();

    final HorizontalPanel location_panel = new HorizontalPanel();
    final Label x_label = new Label("  x: ");
    final Label y_label = new Label("y: ");
    final TextBox x_box = new TextBox();
    final TextBox y_box = new TextBox();

    // Variables for managing the histogram
    final SimplePanel histogram_panel = new SimplePanel();
    ColumnChart histogram;
    Runnable onLoadCallback;

    // Variables for managing the map of logged modules
    final SimplePanel map_panel = new SimplePanel();
    final MarsMap marsMap = new MarsMap(false);

    // The context object
    private final Context context;

    /**
     * Constructor
     *
     * @param context
     */
    public LogModules(final Context context) {
        this.context = context;

        download_text.addStyleName("downloadTextYes");
        download_button.setEnabled(true);
        download_button.addClickHandler(new GPSButton());
        delete_button.addClickHandler(new Delete());

        listBox.addItem("0");
        listBox.addItem("1");
        listBox.addItem("2");
        listBox.addItem("3");
        listBox.addItem("4");
        listBox.addItem("5");
        listBox.addItem("6");
        listBox.addItem("7");
        listBox.addItem("8");
        listBox.addItem("9");
        listBox.addItem("10");

        download_panel.addStyleName("downloadPanel");
        download_panel.add(download_text);
        download_panel.add(listBox);
        download_panel.add(download_button);
        download_panel.add(delete_button);

        title.addStyleName("logHeader");

        // Setup the options for picking an orientation
        orientation_box.addItem("no turns required");
        orientation_box.addItem("one turn required");
        orientation_box.addItem("two turns required");
        orientation_box.setVisibleItemCount(3);

        // Setup the options for picking a usability status
        status_box.addItem("undamaged");
        status_box.addItem("damaged");
        status_box.addItem("uncertain");
        status_box.setVisibleItemCount(3);

        // Configure the x, y, and z coordinate boxes
        x_box.setVisibleLength(8);
        y_box.setVisibleLength(8);
        // z_box.setVisibleLength(8);

        // Add the x, y, and z coordinate lables and boxes to a panel
        location_panel.add(x_label);
        location_panel.add(x_box);
        location_panel.add(y_label);
        location_panel.add(y_box);
        // location_panel.add(z_label);
        // location_panel.add(z_box);

        // Attach a button click handler for logging modules
        submit.addClickHandler(new Submit());

        // Add all of the widgets to the table
        table.setWidget(0, 0, type_label);
        table.setWidget(0, 1, type_box);

        table.setWidget(1, 0, orientation_label);
        table.setWidget(1, 1, orientation_box);

        table.setWidget(2, 0, status_label);
        table.setWidget(2, 1, status_box);

        table.setWidget(3, 0, location_label);
        table.setWidget(3, 1, location_panel);

        table.setWidget(4, 0, submit);
        table.getFlexCellFormatter().setColSpan(4, 0, 2);

        table.addStyleName("logTable");

        // Set the size of the histogram panel
        histogram_panel.setSize("600px", "500px");

        // Setup the map of modules
        map_panel.add(marsMap);

        // Add the title, table, and histogram panel to the vertical panel
        vp.add(download_panel);
        vp.add(title);
        vp.add(table);
        vp.add(histogram_panel);
        vp.add(map_panel);

        // Create a histogram
        onLoadCallback = new Runnable() {
            @Override
            public void run() {
                histogram = new ColumnChart(createTable(), createOptions());

                // Add the histogram to the vertical panel
                histogram_panel.add(histogram);
            }
        };

        // Load the histogram
        VisualizationUtils.loadVisualizationApi(onLoadCallback, ColumnChart.PACKAGE);

        initWidget(vp);
        addStyleName("logModules");
    }

    @Override
    public void onLoad() {
        update();
    }

    /**
     * Override the default toString() method
     */
    @Override
    public String toString() {
        return "Log Modules class";
    }

    private class Delete implements ClickHandler {
        @Override
        public void onClick(ClickEvent event) {
            // Delete all of the logged modules
            for (Module module : context.core.getModules()) {
                context.core.removeSavedModule(module);
            }

            // Delete all of the saved configurations
            for (Configuration config : context.core.getSavedConfigurations()) {
                context.core.removeSavedConfiguration(config);
            }

            //
            Storage storage = Storage.getLocalStorageIfSupported();
            if (storage != null) {
                String date = context.core.getFirstRunDate();
                storage.clear();
                storage.setItem("date", date);
            }
            // Update the page
            update();
            marsMap.clearMap();
        }
    }

    /**
     * Handles button clicks for logging modules.
     */
    private class Submit implements ClickHandler {

        @Override
        public void onClick(final ClickEvent event) {
            // Variables for information entered by the user
            int code;
            final int orientation = orientation_box.getSelectedIndex();
            ;
            final int status = status_box.getSelectedIndex();
            int x, y;

            // Check that input consists of numbers
            try {
                code = Integer.parseInt(type_box.getValue());
                x = Integer.parseInt(x_box.getValue());
                y = Integer.parseInt(y_box.getValue());
                // z = Integer.parseInt(z_box.getValue());
            } catch (final NumberFormatException e) {
                Window.alert("Error: please use numbers for the module type and its coordinates.");
                return;
            }

            // Check the input for sanity
            if (code <= 0 || code > 190) {
                Window.alert("Error: please enter a module type between 1 and 190.");
                return;
            }
            if (orientation != 0 && orientation != 1 && orientation != 2) {
                Window.alert("Error: please choose one of the three orientations available.");
                return;
            }
            if (status != 0 && status != 1 && status != 2) {
                Window.alert("Error: please choose one of the three statuses available.");
                return;
            }
            if (x <= 0 || x > 100 || y <= 0 || y > 50) {
                Window.alert("Error: the coordinates must be contained within a 100x50 grid.");
                return;
            }
            for (final Module m : context.core.getModules()) {
                if (m.getCode() == code) {
                    Window.alert("Error: module code number already entered.");
                    return;
                }
                if (m.getPosition()[0] == x && m.getPosition()[1] == y) {
                    Window.alert("Error: module already logged at this position.");
                    return;
                }
            }

            // Check input to ensure module is not reserved for future use
            if (code >= 41 && code <= 60
                    || code >= 81 && code <= 90
                    || code >= 101 && code <= 110
                    || code >= 121 && code <= 130
                    || code >= 135 && code <= 140
                    || code >= 145 && code <= 150
                    || code >= 155 && code <= 160
                    || code >= 165 && code <= 170
                    || code >= 175 && code <= 180
                    || code >= 185 && code <= 190) {
                Window.alert("Error: cannot log module that is reserved for future use");
                return;
            }

            // Log the module
            logModule(code, orientation, status, x, y, 0);

            // Update the histogram and map
            update();

            // Play sound file
            SoundController soundController = new SoundController();
            final Sound sound = soundController.createSound(Sound.MIME_TYPE_AUDIO_BASIC, "audio/ModuleLogged.MP3");
            sound.play();

            // Check whether a minimum configuration is possible
            // TODO: check when a minimum configuration is possible
            if (true) {
                // Window.alert("A minimum configuration is now possible");
            }
        }
    }

    /**
     * TODO: implement the ability to log a module
     *
     * @param code
     * @param orientation
     * @param status
     * @param x
     * @param y
     * @param z
     */
    private void logModule(
            final int code, final int orientation, final int status_number, final int x, final int y, final int z) {
        assert orientation == 0 || orientation == 1 || orientation == 2;

        // Check input to ensure module is not reserved for future use
        if (code >= 41 && code <= 60
                || code >= 81 && code <= 90
                || code >= 101 && code <= 110
                || code >= 121 && code <= 130
                || code >= 135 && code <= 140
                || code >= 145 && code <= 150
                || code >= 155 && code <= 160
                || code >= 165 && code <= 170
                || code >= 175 && code <= 180
                || code >= 185 && code <= 190) {
            Window.alert("Adding reserved module?" + code + " x:" + x + " y:" + y);
            return;
        }

        Status status;
        if (status_number == 0) {
            status = Status.UNDAMAGED;
        } else if (status_number == 1) {
            status = Status.DAMAGED;
        } else if (status_number == 2) {
            status = Status.DAMAGED;
        } else {
            throw new Error("Error: invalid status");
        }

        context.core.addModule(new Module(code, new int[] {x, y, z}, status, orientation));
    }

    /**
     * Generate fake data to display in the histogram. TODO: use actual module
     * data
     *
     * @return data for histogram
     */
    private AbstractDataTable createTable() {
        final DataTable data = DataTable.create();
        data.addColumn(ColumnType.STRING, "Module Types");
        data.addColumn(ColumnType.NUMBER, "Number of Modules");
        data.addRows(10);
        data.setValue(0, 0, "Plain");
        data.setValue(0, 1, getNumberOfType(Type.PLAIN));
        data.setValue(1, 0, "Dormitory");
        data.setValue(1, 1, getNumberOfType(Type.DORMITORY));
        data.setValue(2, 0, "Sanitation");
        data.setValue(2, 1, getNumberOfType(Type.SANITATION));
        data.setValue(3, 0, "Food & Water");
        data.setValue(3, 1, getNumberOfType(Type.FOODWATER));
        data.setValue(4, 0, "Gym & Relaxation");
        data.setValue(4, 1, getNumberOfType(Type.GYMRELAX));
        data.setValue(5, 0, "Canteen");
        data.setValue(5, 1, getNumberOfType(Type.CANTEEN));
        data.setValue(6, 0, "Power");
        data.setValue(6, 1, getNumberOfType(Type.POWER));
        data.setValue(7, 0, "Control");
        data.setValue(7, 1, getNumberOfType(Type.CONTROL));
        data.setValue(8, 0, "Airlock");
        data.setValue(8, 1, getNumberOfType(Type.AIRLOCK));
        data.setValue(9, 0, "Medical");
        data.setValue(9, 1, getNumberOfType(Type.MEDICAL));
        return data;
    }

    private int getNumberOfType(final Type type) {
        int count = 0;
        for (final Module m : context.core.getModules()) {
            if (type.equals(Type.getType(m.getCode()))) {
                count++;
            }
        }
        return count;
    }

    /**
     * Setup the options for displaying the histogram.
     *
     * @return the histogram options
     */
    private Options createOptions() {
        final Options options = Options.create();
        options.setWidth(600);
        options.setHeight(500);
        options.set3D(true);
        options.setTitle("Quantity of Each Module Type");
        options.setLegend(LegendPosition.NONE);
        return options;
    }

    public void update() {
        // Redraw the histogram when it becomes hidden
        if (histogram != null && onLoadCallback != null) {
            histogram_panel.clear();
            VisualizationUtils.loadVisualizationApi(onLoadCallback, ColumnChart.PACKAGE);
        }

        // Draw all of the logged modules on the map
        if (marsMap != null && context.core.getModules().size() >= 1) {
            marsMap.clearMap();
            marsMap.drawModuleList(context.core.getModules(), MarsMap.getLoggedOptions());
        }
    }

    private class GPSButton implements ClickHandler {
        @Override
        public void onClick(final ClickEvent event) {

            final int caseNum = listBox.getSelectedIndex();
            // String proxy = "http://www.d.umn.edu/~walk0429/Proxy.php?url=";
            String url = "http://www.d.umn.edu/~abrooks/SomeTests.php?q=" + caseNum;
            url = URL.encode(url);
            // download_button.setEnabled(false);
            download_text.removeStyleName("downloadTextYes");
            download_text.addStyleName("downloadTextNo");

            if (caseNum == 0) {
                final String sAll = "["
                        + "{'code':1,'status':0,'turns':0,'X':5,'Y':5},"
                        + "{'code':2,'status':1,'turns':0,'X':5,'Y':6},"
                        + "{'code':3,'status':2,'turns':0,'X':5,'Y':7},"
                        + "{'code':4,'status':0,'turns':1,'X':2,'Y':7},"
                        + "{'code':5,'status':0,'turns':2,'X':76,'Y':34},"
                        + "{'code':6,'status':1,'turns':0,'X':12,'Y':34},"
                        + "{'code':7,'status':0,'turns':2,'X':62,'Y':26},"
                        + "{'code':8,'status':2,'turns':0,'X':1,'Y':1},"
                        + "{'code':9,'status':2,'turns':0,'X':65,'Y':29},"
                        + "{'code':10,'status':0,'turns':1,'X':15,'Y':47},"
                        + "{'code':11,'status':0,'turns':0,'X':76,'Y':32},"
                        + "{'code':12,'status':0,'turns':2,'X':12,'Y':1},"
                        + "{'code':13,'status':0,'turns':1,'X':21,'Y':11},"
                        + "{'code':14,'status':1,'turns':0,'X':22,'Y':22},"
                        + "{'code':15,'status':0,'turns':2,'X':23,'Y':33},"
                        + "{'code':16,'status':2,'turns':1,'X':26,'Y':44},"
                        + "{'code':17,'status':0,'turns':1,'X':55,'Y':32},"
                        + "{'code':18,'status':0,'turns':1,'X':66,'Y':11},"
                        + "{'code':19,'status':0,'turns':2,'X':77,'Y':7},"
                        + "{'code':20,'status':0,'turns':0,'X':88,'Y':3},"
                        + "{'code':21,'status':1,'turns':1,'X':99,'Y':49},"
                        + "{'code':22,'status':0,'turns':0,'X':41,'Y':10},"
                        + "{'code':23,'status':0,'turns':0,'X':38,'Y':20},"
                        + "{'code':24,'status':0,'turns':2,'X':25,'Y':30},"
                        + "{'code':25,'status':0,'turns':0,'X':16,'Y':40},"
                        + "{'code':26,'status':0,'turns':1,'X':24,'Y':50},"
                        + "{'code':27,'status':0,'turns':0,'X':60,'Y':36},"
                        + "{'code':28,'status':1,'turns':2,'X':70,'Y':12},"
                        + "{'code':29,'status':2,'turns':2,'X':80,'Y':6},"
                        + "{'code':30,'status':0,'turns':1,'X':90,'Y':5},"
                        + "{'code':31,'status':0,'turns':0,'X':63,'Y':10},"
                        + "{'code':32,'status':0,'turns':0,'X':78,'Y':20},"
                        + "{'code':33,'status':0,'turns':0,'X':89,'Y':30},"
                        + "{'code':34,'status':0,'turns':2,'X':40,'Y':23},"
                        + "{'code':35,'status':0,'turns':1,'X':50,'Y':45},"
                        + "{'code':36,'status':0,'turns':0,'X':51,'Y':44},"
                        + "{'code':37,'status':0,'turns':0,'X':81,'Y':33},"
                        + "{'code':38,'status':1,'turns':0,'X':22,'Y':48},"
                        + "{'code':39,'status':0,'turns':2,'X':91,'Y':11},"
                        + "{'code':40,'status':0,'turns':1,'X':81,'Y':29},"
                        + "{'code':61,'status':0,'turns':1,'X':99,'Y':44},"
                        + "{'code':62,'status':0,'turns':0,'X':88,'Y':33},"
                        + "{'code':63,'status':0,'turns':0,'X':77,'Y':22},"
                        + "{'code':64,'status':0,'turns':2,'X':66,'Y':11},"
                        + "{'code':65,'status':1,'turns':1,'X':55,'Y':1},"
                        + "{'code':66,'status':0,'turns':0,'X':2,'Y':44},"
                        + "{'code':67,'status':2,'turns':0,'X':3,'Y':33},"
                        + "{'code':68,'status':2,'turns':2,'X':4,'Y':22},"
                        + "{'code':69,'status':1,'turns':2,'X':5,'Y':11},"
                        + "{'code':70,'status':0,'turns':1,'X':98,'Y':1},"
                        + "{'code':71,'status':0,'turns':0,'X':87,'Y':7},"
                        + "{'code':72,'status':0,'turns':0,'X':76,'Y':8},"
                        + "{'code':73,'status':0,'turns':1,'X':65,'Y':9},"
                        + "{'code':74,'status':0,'turns':0,'X':54,'Y':10},"
                        + "{'code':75,'status':0,'turns':2,'X':11,'Y':43},"
                        + "{'code':76,'status':0,'turns':0,'X':12,'Y':32},"
                        + "{'code':77,'status':0,'turns':2,'X':13,'Y':21},"
                        + "{'code':78,'status':0,'turns':0,'X':14,'Y':19},"
                        + "{'code':79,'status':1,'turns':1,'X':67,'Y':15},"
                        + "{'code':80,'status':0,'turns':0,'X':16,'Y':23},"
                        + "{'code':91,'status':0,'turns':0,'X':100,'Y':42},"
                        + "{'code':92,'status':0,'turns':0,'X':99,'Y':41},"
                        + "{'code':93,'status':1,'turns':1,'X':98,'Y':40},"
                        + "{'code':94,'status':2,'turns':1,'X':97,'Y':39},"
                        + "{'code':95,'status':0,'turns':0,'X':96,'Y':38},"
                        + "{'code':96,'status':0,'turns':2,'X':95,'Y':37},"
                        + "{'code':97,'status':0,'turns':2,'X':94,'Y':36},"
                        + "{'code':98,'status':0,'turns':0,'X':93,'Y':35},"
                        + "{'code':99,'status':0,'turns':0,'X':92,'Y':34},"
                        + "{'code':100,'status':0,'turns':0,'X':91,'Y':33},"
                        + "{'code':111,'status':0,'turns':0,'X':12,'Y':32},"
                        + "{'code':112,'status':0,'turns':0,'X':65,'Y':23},"
                        + "{'code':113,'status':0,'turns':1,'X':34,'Y':23},"
                        + "{'code':114,'status':0,'turns':2,'X':65,'Y':45},"
                        + "{'code':115,'status':1,'turns':1,'X':43,'Y':49},"
                        + "{'code':116,'status':0,'turns':1,'X':32,'Y':1},"
                        + "{'code':117,'status':0,'turns':0,'X':89,'Y':21},"
                        + "{'code':118,'status':0,'turns':0,'X':78,'Y':1},"
                        + "{'code':119,'status':0,'turns':1,'X':6,'Y':43},"
                        + "{'code':120,'status':2,'turns':0,'X':51,'Y':39},"
                        + "{'code':131,'status':0,'turns':0,'X':84,'Y':45},"
                        + "{'code':132,'status':0,'turns':0,'X':23,'Y':49},"
                        + "{'code':133,'status':0,'turns':0,'X':81,'Y':29},"
                        + "{'code':134,'status':2,'turns':0,'X':51,'Y':29},"
                        + "{'code':141,'status':0,'turns':1,'X':1,'Y':19},"
                        + "{'code':142,'status':0,'turns':2,'X':2,'Y':28},"
                        + "{'code':143,'status':2,'turns':0,'X':3,'Y':37},"
                        + "{'code':144,'status':0,'turns':1,'X':4,'Y':46},"
                        + "{'code':151,'status':0,'turns':2,'X':55,'Y':5},"
                        + "{'code':152,'status':0,'turns':0,'X':91,'Y':6},"
                        + "{'code':153,'status':1,'turns':1,'X':82,'Y':7},"
                        + "{'code':154,'status':0,'turns':0,'X':73,'Y':11},"
                        + "{'code':161,'status':0,'turns':1,'X':64,'Y':22},"
                        + "{'code':162,'status':0,'turns':2,'X':55,'Y':33},"
                        + "{'code':163,'status':0,'turns':1,'X':44,'Y':12},"
                        + "{'code':164,'status':0,'turns':0,'X':12,'Y':23},"
                        + "{'code':171,'status':0,'turns':2,'X':23,'Y':34},"
                        + "{'code':172,'status':0,'turns':2,'X':34,'Y':45},"
                        + "{'code':173,'status':1,'turns':1,'X':56,'Y':44},"
                        + "{'code':174,'status':0,'turns':0,'X':67,'Y':27},"
                        + "{'code':181,'status':0,'turns':1,'X':78,'Y':21},"
                        + "{'code':182,'status':0,'turns':0,'X':89,'Y':46},"
                        + "{'code':183,'status':0,'turns':0,'X':99,'Y':28},"
                        + "{'code':184,'status':2,'turns':1,'X':100,'Y':12}"
                        + "]";
                final Storage moduleStore = Storage.getLocalStorageIfSupported();

                if (moduleStore != null) {
                    moduleStore.setItem("localMods", sAll);
                }
                final String sConfigOne = moduleStore.getItem("localMods");
                final JSONArray jA = (JSONArray) JSONParser.parseLenient(sConfigOne);
                JSONNumber jCode;
                JSONNumber jStatus;
                JSONNumber jTurns;
                JSONNumber jX;
                JSONNumber jY;
                double code;
                double status;
                double turns;
                double xCoor;
                double yCoor;

                for (int i = 0; i < jA.size(); i++) {
                    final JSONObject jO = (JSONObject) jA.get(i);
                    jCode = (JSONNumber) jO.get("code");
                    code = jCode.doubleValue();
                    jStatus = (JSONNumber) jO.get("status");
                    status = jStatus.doubleValue();
                    jTurns = (JSONNumber) jO.get("turns");
                    turns = jTurns.doubleValue();
                    jX = (JSONNumber) jO.get("X");
                    xCoor = jX.doubleValue();
                    jY = (JSONNumber) jO.get("Y");
                    yCoor = jY.doubleValue();

                    // logModule((int) code, (int) status, (int) turns,
                    // (int) xCoor, (int) yCoor, 0);
                    logModule((int) code, (int) turns, (int) status, (int) xCoor, (int) yCoor, 0);
                }
                update();

                // Play sound file
                SoundController soundController = new SoundController();
                final Sound sound =
                        soundController.createSound(Sound.MIME_TYPE_AUDIO_BASIC, "audio/DownloadComplete.MP3");
                sound.play();

            } else if (caseNum == 1
                    || caseNum == 2
                    || caseNum == 3
                    || caseNum == 4
                    || caseNum == 5
                    || caseNum == 6
                    || caseNum == 7
                    || caseNum == 8
                    || caseNum == 9
                    || caseNum == 10) {
                final RequestBuilder builder = new RequestBuilder(RequestBuilder.GET, url);
                try {
                    builder.sendRequest(null, new RequestCallback() {
                        @Override
                        public void onError(final Request request, final Throwable exception) {
                            Window.alert("onError: Couldn't retrieve JSON");
                        }

                        @Override
                        public void onResponseReceived(final Request request, final Response response) {
                            if (200 == response.getStatusCode()) {
                                final String rt = response.getText();
                                final JSONArray jA = (JSONArray) JSONParser.parseLenient(rt);
                                JSONNumber jCode;
                                JSONString jStatus;
                                JSONNumber jTurns;
                                JSONNumber jX;
                                JSONNumber jY;
                                double code;
                                String status;
                                double turns;
                                double xCoor;
                                double yCoor;

                                for (int i = 0; i < jA.size(); i++) {
                                    final JSONObject jO = (JSONObject) jA.get(i);
                                    jCode = (JSONNumber) jO.get("code");
                                    code = jCode.doubleValue();
                                    jStatus = (JSONString) jO.get("status");
                                    status = jStatus.stringValue();
                                    jTurns = (JSONNumber) jO.get("turns");
                                    turns = jTurns.doubleValue();
                                    jX = (JSONNumber) jO.get("X");
                                    xCoor = jX.doubleValue();
                                    jY = (JSONNumber) jO.get("Y");
                                    yCoor = jY.doubleValue();
                                    int statusAsInt = -1;
                                    boolean goodStatus;

                                    if (status.equals("undamaged")) {
                                        statusAsInt = 0;
                                        goodStatus = true;
                                    } else if (status.equals("damaged")) {
                                        statusAsInt = 1;
                                        goodStatus = true;
                                    } else if (status.equals("repair") || status.equals("uncertain")) {
                                        statusAsInt = 1;
                                        goodStatus = true;
                                    } else {
                                        Window.alert("Status is unrecognized");
                                        goodStatus = false;
                                    }
                                    if (goodStatus) {
                                        // logModule((int) code,
                                        // statusAsInt,
                                        // (int) turns,
                                        // (int) xCoor,
                                        // (int) yCoor, 0);
                                        logModule((int) code, (int) turns, statusAsInt, (int) xCoor, (int) yCoor, 0);
                                    }
                                }

                                // Play sound file
                                SoundController soundController = new SoundController();
                                final Sound sound = soundController.createSound(
                                        Sound.MIME_TYPE_AUDIO_BASIC, "audio/DownloadComplete.MP3");
                                sound.play();
                            } else {
                                Window.alert("Couldn't retrieve JSON (" + response.getStatusText() + ")");
                            }
                            update();
                        }
                    });
                } catch (final RequestException e) {
                    Window.alert("RequestException: Couldn't retrieve JSON");
                }
            } else {
                Window.alert("Invalid test case entered! Try again!");
            }
        }
    }
}
