package racoons.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Anchor;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * A class for showing the user a menu on the left hand side of every page.
 *
 * @author Tim and James
 */
public class Menu extends Composite {

    public Menu(final Context context) {
        final VerticalPanel vp = new VerticalPanel();

        // Create links
        final Anchor log = new Anchor("Log modules");
        final Anchor display = new Anchor("Display configurations");
        final Anchor edit = new Anchor("Edit configurations");
        final Anchor logout = new Anchor("Logout");
        final Weather weather = new Weather();
        final DateWidget dateWidget = new DateWidget(context);

        // Add handlers to each link
        log.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                context.switchView(Context.View.LOGMODULES);
            }
        });

        display.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                context.switchView(Context.View.DISPLAYLIST);
            }
        });

        edit.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                context.switchView(Context.View.EDITCONFIGURATION);
            }
        });

        logout.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                context.switchView(Context.View.LOGIN);
            }
        });

        // Add links to panel
        vp.add(log);
        vp.add(display);
        vp.add(edit);
        vp.add(logout);
        vp.add(weather);
        vp.add(dateWidget);
        // setStyleName("menu");
        initWidget(vp);

        addStyleName("menu");
    }

    @Override
    public String toString() {
        return "Menu class";
    }
}
