package racoons.client;

import backend.common.Configuration;
import backend.common.Module;
import com.google.gwt.dom.client.Document;
import com.google.gwt.maps.client.*;
import com.google.gwt.maps.client.base.LatLng;
import com.google.gwt.maps.client.base.LatLngBounds;
import com.google.gwt.maps.client.mvc.MVCArray;
import com.google.gwt.maps.client.overlays.*;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import java.util.LinkedList;
import java.util.List;

/**
 * A class for showing a map of Mars. It is used as a wrapper for a Google Maps
 * object, translating coordinates and drawing modules.
 *
 * @author Tim
 */
public class MarsMap extends Composite {

    // Bounds for map, in Australia to simulate Mars surface
    private static final LatLng SE = LatLng.newInstance(-25.891349, 136.579971);
    private static final LatLng NE = LatLng.newInstance(-24.142994, 136.579971);
    private static final LatLng SW = LatLng.newInstance(-25.891349, 134.476776);
    private static final LatLng NW = LatLng.newInstance(-24.142994, 134.476776);

    private final LatLngBounds allowedBounds = LatLngBounds.newInstance(SW, NE);

    private final LatLng center = allowedBounds.getCenter();

    // Variables for widgets
    private final HTML div_html = new HTML(DIV);

    // Variables for Google maps
    private final MapOptions mapOptions = MapOptions.newInstance();
    private static final String DIV = "<div id=\"map_canvas\" style=\"" + "width:100%; height:600px\">map here</div>";
    private MapWidget map;
    private LatLng lastValidCenter;

    // Last drawn lines to reset them
    private final List<Polyline> lines = new LinkedList<>();
    private final List<Polyline> outlines = new LinkedList<>();
    private final List<GroundOverlay> overlays = new LinkedList<>();
    private final List<InfoWindow> infowindows = new LinkedList<>();

    // Editing variables
    private final boolean edit;
    private List<Module> modules = new LinkedList<>();
    private int selectedCode = -1;

    public MarsMap(boolean edit) {
        this.edit = edit;

        // Configure the map
        mapOptions.setCenter(center);
        mapOptions.setZoom(10);
        mapOptions.setMapTypeId(MapTypeId.SATELLITE);
        mapOptions.setMaxZoom(12);
        mapOptions.setMinZoom(8);
        mapOptions.setMapTypeControl(false);
        mapOptions.setStreetViewControl(false);
        mapOptions.setDisableDoubleClickZoom(true);

        // Housekeeping
        initWidget(div_html);
        addStyleName("marsMap");
    }

    /**
     * Removes objects drawn on the map.
     */
    public void clearMap() {
        clearLines();
        clearOutlines();
        clearModules();
    }

    /**
     * Clears all lines associated with paths on the map.
     */
    public void clearLines() {
        // Destroy all of the polylines
        for (final Polyline line : lines) {
            line.setMap(null);
        }
        lines.clear();
    }

    /**
     * Clears all lines associated with module outlines on the map.
     */
    public void clearOutlines() {
        // Destroy all of the polylines
        for (final Polyline outline : outlines) {
            outline.setMap(null);
        }
        outlines.clear();
    }

    /**
     * Removes all GroundOverlays associated with modules on the map.
     */
    public void clearModules() {
        // Destroy all module images
        for (final GroundOverlay gov : overlays) {
            gov.setMap(null);
        }

        for (final InfoWindow iw : infowindows) {
            iw.close();
        }

        // Empty both lists
        overlays.clear();
        infowindows.clear();
    }

    /**
     * Called when the DOM is loaded, sets up Google Maps.
     */
    @Override
    protected void onLoad() {
        map = MapWidget.newInstance(MapImpl.newInstance(Document.get().getElementById("map_canvas"), mapOptions));

        map.setTilt(45);
        map.setHeading(90);

        lastValidCenter = map.getCenter();

        // Restrict the panning to a certain range
        map.addCenterChangeHandler(event -> {
            if (allowedBounds.contains(map.getCenter())) {
                lastValidCenter = map.getCenter();
                return;
            }
            map.panTo(lastValidCenter);
        });

        drawMapOutline();
        drawSandyRegion();

        // Add a listener for placing a selected module
        map.addClickHandler(event -> {
            if (selectedCode != -1) {
                for (Module module : modules) {
                    if (module.getCode() == selectedCode) {
                        LatLng newLatLngPosition = event.getMouseEvent().getLatLng();
                        int[] xyCoord = LatLngToCoords(newLatLngPosition);

                        // Make sure no module is currently at this position
                        for (Module anyModule : modules) {
                            if (anyModule.getPosition()[0] == xyCoord[0]
                                    && anyModule.getPosition()[1] == xyCoord[1]) {
                                return;
                            }
                        }

                        // Change the selected modules position
                        module.setXPosition(xyCoord[0]);
                        module.setYPosition(xyCoord[1]);

                        // Redraw all of the modules
                        clearMap();

                        // Get all unselected modules, draw them with a
                        // certain
                        // border
                        List<Module> otherModules = new LinkedList<Module>();
                        for (Module anyModule : modules) {
                            if (selectedCode != anyModule.getCode()) {
                                otherModules.add(anyModule);
                            }
                        }
                        drawModuleList(otherModules, getConfigurationOptions());

                        // Draw the selected module with a different border
                        List<Module> selectedModules = new LinkedList<Module>();
                        selectedModules.add(module);
                        drawModuleList(selectedModules, getLoggedOptions());
                    }
                }
            }
        });
    }

    /**
     * Returns the list of modules after the user has finished editing them by
     * clicking and placing. Not appropriate if edit is false.
     *
     * @return the list of modules that are edited
     */
    public List<Module> getModulesAfterEdits() {
        assert edit;
        return modules;
    }

    /**
     * Draws a given path on the map.
     *
     * @param path
     *            is a list of int[]{x,y,z} coordinates
     */
    public void drawPath(final List<int[]> path) {
        assert path != null;
        assert !path.isEmpty();

        final MVCArray<LatLng> latlng_path = MVCArray.newInstance();

        final PolylineOptions o = PolylineOptions.newInstance();
        o.setMap(map);
        o.setStrokeColor("#000000");
        o.setStrokeOpacity(1.0);
        o.setStrokeWeight(2);
        o.setPath(latlng_path);

        for (final int[] xy_point : path) {
            assert xy_point.length >= 2;

            final LatLng point = coordsToLatLng(xy_point[0], xy_point[1]);
            latlng_path.push(point);
        }

        lines.add(Polyline.newInstance(o));
    }

    /**
     * Draws all of the modules of a configuration on the map.
     *
     * @param config
     */
    public void drawConfiguration(final Configuration config, final PolylineOptions options) {
        assert config != null;
        assert !config.getModules().isEmpty();

        // Draw the modules
        this.modules = config.getModules();
        drawModuleList(config.getModules(), options);

        // Center the map on the first module, if possible
        try {
            map.setCenter(coordsToLatLng(
                    modules.get(0).getPosition()[0], modules.get(0).getPosition()[1]));
        } catch (final Exception ex) {
            System.out.println(ex);
            throw new Error(ex);
        }
    }

    /**
     * Draws all of the modules in a given list on the map.
     *
     * @param modules
     */
    public void drawModuleList(final List<Module> modules, final PolylineOptions options) {
        assert modules != null;

        // If there aren't any modules, don't draw them
        if (modules.isEmpty()) {
            return;
        }

        // Draw all module icons and outlines
        for (final Module m : modules) {
            drawModuleIcon(m);
            drawModuleOutline(m, options);
        }
    }

    /**
     * Returns options used to display logged modules.
     *
     * @return options for the outline of a logged module
     */
    public static PolylineOptions getLoggedOptions() {
        final PolylineOptions options = PolylineOptions.newInstance();
        options.setStrokeColor("#0000FF");
        options.setStrokeOpacity(1.0);
        options.setStrokeWeight(1);
        return options;
    }

    /**
     * Returns options used to display a module that is part of a configuration.
     *
     * @return options for the outline of a configuration module
     */
    public static PolylineOptions getConfigurationOptions() {
        final PolylineOptions options = PolylineOptions.newInstance();
        options.setStrokeColor("#00FF00");
        options.setStrokeOpacity(1.0);
        options.setStrokeWeight(1);
        return options;
    }

    /**
     * Convert x and y coordinates from a 100x50 grid into latitude and
     * longitude coordinates projected on the Martian landscape.
     *
     * @param x
     * @param y
     * @return the LatLng representation of the given xy point
     */
    private LatLng coordsToLatLng(final int x, final int y) {
        // assert x > 0 && x <= 100 && y > 0 && y <= 50;

        final double lat = 0 + Math.abs(y * (NW.getLatitude() - SW.getLatitude()) / 50) + SW.getLatitude();
        final double lng = 0 + Math.abs(x * (NE.getLongitude() - NW.getLongitude()) / 100) + NW.getLongitude();
        return LatLng.newInstance(lat, lng);
    }

    private int[] LatLngToCoords(final LatLng latLng) {
        // Get x and y coordinates in doubles
        double yEstimate =
                50d * (latLng.getLatitude() - SW.getLatitude()) / Math.abs(NW.getLatitude() - SW.getLatitude());
        double xEstimate =
                100d * (latLng.getLongitude() - NW.getLongitude()) / Math.abs(NE.getLongitude() - NW.getLongitude());

        int[] xyCoord = new int[2];

        // Round x and y coordinates to integers
        if ((yEstimate - Math.floor(yEstimate)) < 0.5) {
            xyCoord[1] = (int) Math.floor(yEstimate);
        } else {
            xyCoord[1] = (int) Math.ceil(yEstimate);
        }

        if ((xEstimate - Math.floor(xEstimate)) < 0.5) {
            xyCoord[0] = (int) Math.floor(xEstimate);
        } else {
            xyCoord[0] = (int) Math.ceil(xEstimate);
        }

        // Return the x,y coordinate pair
        return xyCoord;
    }

    private void drawModuleOutline(final Module module, final PolylineOptions options) {
        assert module != null;

        final double WIDTH = Math.abs(NE.getLongitude() - NW.getLongitude()) / 200;
        final double HEIGHT = Math.abs(NE.getLatitude() - SE.getLatitude()) / 100;

        final LatLng center = coordsToLatLng(module.getPosition()[0], module.getPosition()[1]);

        final MVCArray<LatLng> path = MVCArray.newInstance();
        path.push(LatLng.newInstance(center.getLatitude() + HEIGHT, center.getLongitude() + WIDTH));
        path.push(LatLng.newInstance(center.getLatitude() - HEIGHT, center.getLongitude() + WIDTH));
        path.push(LatLng.newInstance(center.getLatitude() - HEIGHT, center.getLongitude() - WIDTH));
        path.push(LatLng.newInstance(center.getLatitude() + HEIGHT, center.getLongitude() - WIDTH));
        path.push(LatLng.newInstance(center.getLatitude() + HEIGHT, center.getLongitude() + WIDTH));

        options.setMap(map);
        options.setPath(path);

        outlines.add(Polyline.newInstance(options));
    }

    private void drawModuleIcon(final Module module) {
        final double WIDTH = Math.abs(NE.getLongitude() - NW.getLongitude()) / 200;
        final double HEIGHT = Math.abs(NE.getLatitude() - SE.getLatitude()) / 100;

        final LatLng center = coordsToLatLng(module.getPosition()[0], module.getPosition()[1]);

        final LatLngBounds bounds = LatLngBounds.newInstance(
                LatLng.newInstance(center.getLatitude() - HEIGHT, center.getLongitude() - WIDTH),
                LatLng.newInstance(center.getLatitude() + HEIGHT, center.getLongitude() + WIDTH));

        final String img = "images/" + module.getType().toString() + ".png";

        final GroundOverlayOptions options = GroundOverlayOptions.newInstance();
        final GroundOverlay groundOverlay = GroundOverlay.newInstance(img, bounds, options);
        groundOverlay.setMap(map);

        // Add click listeners to the overlay image
        addOverlayInfoWindow(groundOverlay, module);
        if (edit) addEditListener(groundOverlay, module);

        overlays.add(groundOverlay);
    }

    private void addOverlayInfoWindow(final GroundOverlay gov, final Module module) {
        final InfoWindowOptions opts = InfoWindowOptions.newInstance();
        opts.setContent("<div id='moduleInfoWindow'>" + "Code:"
                + module.getCode()
                + "</br style='line-height:0px;content:\"\";display:block'>"
                + "Type: " + module.getType()
                + "</br style='line-height:0px;'>" + "Orientation: "
                + module.getOrientation() + "</br style='line-height:0px;'>"
                + "x: " + module.getPosition()[0] + ", \t" + "y: "
                + module.getPosition()[1] + "</br style='line-height:0px;'>"
                + "Status: " + module.getStatus().toString().toLowerCase()
                + "</br style='line-height:0px;'>" + "</div>");
        opts.setPosition(coordsToLatLng(module.getPosition()[0], module.getPosition()[1]));

        if (!edit) {
            gov.addClickHandler(event -> {
                // Remove all prior infowindows
                for (final InfoWindow iw : infowindows) {
                    iw.close();
                }
                infowindows.clear();

                // Add the infowindow to the map
                final InfoWindow iw = InfoWindow.newInstance(opts);
                iw.open(map);
                infowindows.add(iw);
            });
        }
    }

    private void addEditListener(final GroundOverlay gov, final Module module) {
        gov.addClickHandler(event -> {
            if (edit) {
                selectedCode = module.getCode();
                clearMap();

                // Get all unselected modules, draw them with a certain
                // border
                List<Module> otherModules = new LinkedList<Module>();
                for (Module anyModule : modules) {
                    if (selectedCode != anyModule.getCode()) {
                        otherModules.add(anyModule);
                    }
                }
                drawModuleList(otherModules, getConfigurationOptions());

                // Draw the selected module with a different border
                List<Module> selectedModules = new LinkedList<Module>();
                selectedModules.add(module);
                drawModuleList(selectedModules, getLoggedOptions());
            }
        });
    }

    private void drawMapOutline() {

        final double WIDTH = Math.abs(NE.getLongitude() - NW.getLongitude()) / 200;
        final double HEIGHT = Math.abs(NE.getLatitude() - SE.getLatitude()) / 100;

        final MVCArray<LatLng> path = MVCArray.newInstance();
        path.push(LatLng.newInstance(NW.getLatitude() + HEIGHT, NW.getLongitude() + WIDTH));
        path.push(LatLng.newInstance(NE.getLatitude() + HEIGHT, NE.getLongitude() + WIDTH));
        path.push(LatLng.newInstance(SE.getLatitude() + HEIGHT, SE.getLongitude() + WIDTH));
        path.push(LatLng.newInstance(SW.getLatitude() + HEIGHT, SW.getLongitude() + WIDTH));
        path.push(LatLng.newInstance(NW.getLatitude() + HEIGHT, NW.getLongitude() + WIDTH));

        final PolylineOptions o = PolylineOptions.newInstance();
        o.setMap(map);
        o.setStrokeColor("#000000");
        o.setStrokeOpacity(1.0);
        o.setStrokeWeight(2);
        o.setPath(path);

        Polyline.newInstance(o);
    }

    private void drawSandyRegion() {
        final double WIDTH = Math.abs(NE.getLongitude() - NW.getLongitude()) / 200;
        final double HEIGHT = Math.abs(NE.getLatitude() - SE.getLatitude()) / 100;

        final MVCArray<LatLng> path = MVCArray.newInstance();
        path.push(LatLng.newInstance(
                coordsToLatLng(40, 50).getLatitude() + HEIGHT,
                coordsToLatLng(40, 50).getLongitude() - WIDTH));
        path.push(LatLng.newInstance(
                coordsToLatLng(40, 50).getLatitude() + HEIGHT,
                coordsToLatLng(50, 50).getLongitude() + WIDTH));
        path.push(LatLng.newInstance(
                coordsToLatLng(50, 40).getLatitude() - HEIGHT,
                coordsToLatLng(50, 50).getLongitude() + WIDTH));
        path.push(LatLng.newInstance(
                coordsToLatLng(40, 40).getLatitude() - HEIGHT,
                coordsToLatLng(40, 50).getLongitude() - WIDTH));
        path.push(LatLng.newInstance(
                coordsToLatLng(40, 50).getLatitude() + HEIGHT,
                coordsToLatLng(40, 50).getLongitude() - WIDTH));

        final PolylineOptions o = PolylineOptions.newInstance();
        o.setMap(map);
        o.setStrokeColor("#FF0000");
        o.setStrokeOpacity(1.0);
        o.setStrokeWeight(2);
        o.setPath(path);

        Polyline.newInstance(o);
    }
}
