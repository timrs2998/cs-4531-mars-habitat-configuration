package racoons.client;

import com.google.gwt.core.client.EntryPoint;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Entry implements EntryPoint {

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {
        Context context = new Context();
        context.switchView(Context.View.LOGIN);
    }
}
