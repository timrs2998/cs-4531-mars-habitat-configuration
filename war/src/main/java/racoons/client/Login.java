package racoons.client;

import com.allen_sauer.gwt.voices.client.Sound;
import com.allen_sauer.gwt.voices.client.SoundController;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextBox;

/**
 * A class for displaying a widget that allows the user to login.
 *
 * @author Tim
 */
public class Login extends Composite {
    final Context context;

    // Variables for the widgets
    final FlexTable table = new FlexTable();
    final Label greeting = new Label("Please login below:");
    final TextBox username = new TextBox();
    final TextBox password = new TextBox();
    final Button login = new Button("Login");

    /**
     * Constructor
     *
     * @param context
     */
    public Login(final Context context) {
        this.context = context;

        // Set the default text to display
        username.setText("Username");
        password.setText("Password");

        // Add a click handler to login button
        login.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                // Get the user input
                final String user = username.getValue();
                final String pass = password.getValue();

                // Check if it's the correct user
                if (user.equals("Username") && pass.equals("Password")) {
                    context.switchView(Context.View.LOGMODULES);

                    // Play sound file
                    SoundController soundController = new SoundController();
                    final Sound sound =
                            soundController.createSound(Sound.MIME_TYPE_AUDIO_BASIC, "audio/LogInSuccessful.MP3");
                    sound.play();
                } else {
                    // Play sound file
                    SoundController soundController = new SoundController();
                    final Sound sound =
                            soundController.createSound(Sound.MIME_TYPE_AUDIO_BASIC, "audio/LogInFailed.MP3");
                    sound.play();
                    Window.alert("Error: invalid username and password");
                    username.setText("Username");
                    password.setText("Password");
                }
            }
        });

        // Add elements to table
        table.setWidget(0, 0, greeting);
        table.setWidget(1, 0, username);
        table.setWidget(2, 0, password);
        table.setWidget(3, 0, login);

        initWidget(table);
        addStyleName("login");

        // Focus the cursor on the name field when the app loads
        username.setFocus(true);
        username.selectAll();
    }

    /**
     * Override default toString() method
     */
    @Override
    public String toString() {
        return "Login class";
    }
}
