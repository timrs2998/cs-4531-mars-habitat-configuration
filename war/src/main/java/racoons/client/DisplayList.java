package racoons.client;

import backend.common.Configuration;
import backend.common.Configuration.Rule;
import com.allen_sauer.gwt.voices.client.Sound;
import com.allen_sauer.gwt.voices.client.SoundController;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.CheckBox;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.TextArea;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.LinkedList;
import java.util.List;

/**
 * A class for displaying the list of generated configurations, their quality,
 * and providing the functionality of saving and loading configurations.
 *
 * @author Tim
 */
public class DisplayList extends Composite {

    // Variables for widgets
    private final transient VerticalPanel vp = new VerticalPanel();
    private final transient Label title = new Label("Generated configurations depicted in a list");
    private final transient FlexTable table_generated = new FlexTable();
    private final transient Button save = new Button("Save checked configurations");
    private final transient Label saved_title = new Label("Saved configurations depicted in a list");
    private final transient FlexTable table_saved = new FlexTable();

    // Variables for showing violated rules
    private final transient TextArea rulesBox = new TextArea();

    // Variable for holding list of configurations displayed in the table
    private transient List<Configuration> generated_list = new LinkedList<Configuration>();
    private transient List<Configuration> saved_list = new LinkedList<Configuration>();
    private transient List<CheckBox> checkboxes = new LinkedList<CheckBox>();

    // Variable for map section
    private final transient DisplayMap map;

    // The context variable
    private final Context context;

    /**
     * Constructor
     *
     * @param context
     */
    public DisplayList(final Context context) {
        super();
        this.context = context;

        // Initialize map
        map = new DisplayMap(context);

        title.addStyleName("listHeader");

        update_generated();
        table_generated.addClickHandler(new GeneratedTableClickHandler());
        table_generated.addStyleName("listTable");

        // Add a click handler to save checked configurations
        save.addClickHandler(new SaveClickHandler());

        saved_title.addStyleName("listHeader");

        update_saved();
        table_saved.addClickHandler(new SavedTableClickHandler());
        table_saved.addStyleName("listTable");

        rulesBox.setEnabled(true);
        rulesBox.setReadOnly(true);
        rulesBox.setWidth("500px");
        rulesBox.setHeight("200px");

        // Add widgets to the vertical panel
        vp.add(title);
        vp.add(table_generated);
        vp.add(save);
        vp.add(saved_title);
        vp.add(table_saved);
        vp.add(rulesBox);
        vp.add(map);

        // Housekeeping
        initWidget(vp);
        addStyleName("displayList");

        if (saved_list.size() > 0 || generated_list.size() > 0) {
            // Play sound file
            SoundController soundController = new SoundController();
            final Sound sound = soundController.createSound(Sound.MIME_TYPE_AUDIO_BASIC, "audio/ConfigLoad.MP3");
            sound.play();
        }
    }

    /**
     * Creates all the table entries.
     */
    private final void update_generated() {
        table_generated.clear(); // Calculate configurations
        checkboxes.clear();

        // Initialize header row in table
        table_generated.setText(0, 0, "Save");
        table_generated.setText(0, 1, "Quality metric");
        table_generated.setText(0, 2, "Code numbers and position coords of modules (code, x, y, z)");

        generated_list = context.core.getConfigurations();

        // // Sort the list in reverse order
        // Collections.sort(generated_list, new Comparator<Configuration>() {
        // @Override
        // public int compare(final Configuration arg0,
        // final Configuration arg1) {
        // return ((Double) arg1.getConformanceToRules()).compareTo(arg0
        // .getConformanceToRules());
        // }
        //
        // });

        // Add all elements in the list to the table
        for (int i = 1; i <= generated_list.size(); i++) {
            final CheckBox cb = new CheckBox("");
            checkboxes.add(cb);

            table_generated.setWidget(i, 0, cb);
            table_generated.setText(i, 1, generated_list.get(i - 1).getConformanceToRules() * 10 + "");
            table_generated.setText(i, 2, generated_list.get(i - 1).toString());
        }

        table_generated.getRowFormatter().addStyleName(0, "listTableHead");
        for (int i = 1; i <= generated_list.size(); i++) {
            table_generated.getRowFormatter().setStyleName(i, "listTableRow");
        }
    }

    private final void update_saved() {
        table_saved.clear();

        saved_list = context.core.getSavedConfigurations();

        // Initialize header row in table
        table_saved.setText(0, 0, "Quality metric");
        table_saved.setText(0, 1, "Code numbers and position coords of modules (code, x, y, z)");

        // Sort the list in reverse order
        // Collections.sort(saved_list, new Comparator<Configuration>() {
        // @Override
        // public int compare(final Configuration arg0,
        // final Configuration arg1) {
        // return ((Double) arg1.getConformanceToRules()).compareTo(arg0
        // .getConformanceToRules());
        // }
        //
        // });

        // Add all elements in the list to the table
        for (int i = 1; i <= saved_list.size(); i++) {
            table_saved.setText(i, 0, saved_list.get(i - 1).getConformanceToRules() * 10 + "");
            table_saved.setText(i, 1, saved_list.get(i - 1).toString());
        }

        table_saved.getRowFormatter().addStyleName(0, "listTableHead");
        for (int i = 1; i <= saved_list.size(); i++) {
            table_saved.getRowFormatter().setStyleName(i, "listTableRow");
        }
    }

    /**
     * Class used as click handler for the table of module configurations.
     */
    private class GeneratedTableClickHandler implements ClickHandler {
        /**
         * Called when a configuration is clicked in the table.
         */
        @Override
        public void onClick(final ClickEvent event) {
            try {
                final int row = table_generated.getCellForEvent(event).getRowIndex();

                // Ensure that title row wasn't clicked
                if (row != 0) {
                    // Change the map displayed
                    map.setConfiguration(generated_list.get(row - 1));

                    final List<Rule> rules = generated_list.get(row - 1).getViolatedRules();

                    String ruleStrings = "";
                    for (final Rule rule : rules) {
                        ruleStrings += rule.toString() + "\n\n";
                    }
                    rulesBox.setText(ruleStrings);

                    // Play sound file
                    SoundController soundController = new SoundController();
                    final Sound sound =
                            soundController.createSound(Sound.MIME_TYPE_AUDIO_BASIC, "audio/ConfigDisplayed.MP3");
                    sound.play();
                }
            } catch (final Exception e) {
            }
        }
    }

    private class SavedTableClickHandler implements ClickHandler {

        @Override
        public void onClick(final ClickEvent event) {
            try {
                final int row = table_saved.getCellForEvent(event).getRowIndex();

                // Ensure that title row wasn't clicked
                if (row != 0) {
                    // Change the map displayed
                    map.setConfiguration(saved_list.get(row - 1));

                    final List<Rule> rules = saved_list.get(row - 1).getViolatedRules();

                    String ruleStrings = "";
                    for (final Rule rule : rules) {
                        ruleStrings += rule.toString() + "\n";
                    }
                    rulesBox.setText(ruleStrings);

                    // Play sound file
                    SoundController soundController = new SoundController();
                    final Sound sound =
                            soundController.createSound(Sound.MIME_TYPE_AUDIO_BASIC, "audio/ConfigDisplayed.MP3");
                    sound.play();
                }
            } catch (final Exception e) {
            }
        }
    }

    /**
     * Class used as click handler for the save configurations button.
     */
    private class SaveClickHandler implements ClickHandler {

        /**
         * Called when the user wishes to save his/her checked configurations
         */
        @Override
        public void onClick(final ClickEvent event) {
            // Save every checked configuration
            for (int i = 0; i < checkboxes.size(); i++) {
                final Configuration current_config = generated_list.get(i);

                if (checkboxes.get(i).getValue()
                        && !context.core.getSavedConfigurations().contains(current_config)) {
                    context.core.saveConfiguration(current_config);

                    // Play sound file
                    SoundController soundController = new SoundController();
                    final Sound sound =
                            soundController.createSound(Sound.MIME_TYPE_AUDIO_BASIC, "audio/ConfigSaved.MP3");
                    sound.play();
                }
            }
            update_generated();
            update_saved();
        }
    }
}
