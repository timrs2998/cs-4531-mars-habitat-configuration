package racoons.client;

import backend.common.Core;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.RootPanel;

/**
 * A class that controls what the user sees, while providing access to the
 * public functions of other visual objects and the core.
 *
 * @author Tim
 */
public class Context {
    private final HorizontalPanel hp;

    // A backend object
    public Core core;

    // Screens that can be displayed
    public Login login;
    public LogModules logModules;
    public Menu menu;
    public DisplayList displayList;
    public EditConfiguration editConf;

    /**
     * Sole constructor.
     */
    public Context() {
        hp = new HorizontalPanel();
        core = new Core();
    }

    /**
     * Changes what the user sees to the desired module. It knows whether or not
     * the menu should be shown or not, as well as whether to call update() on
     * certain objects.
     *
     * @param comp
     *            the component to display
     */
    public void switchView(final View view) {

        hp.clear();
        RootPanel.get("content").clear();

        if (view == View.LOGIN) {
            login = new Login(this);
            RootPanel.get("content").add(login);
        } else {
            if (menu == null) {
                menu = new Menu(this);
            }
            hp.add(menu);

            if (view == View.DISPLAYLIST) {
                displayList = new DisplayList(this);
                hp.add(displayList);
            } else if (view == View.LOGMODULES) {
                logModules = new LogModules(this);
                hp.add(logModules);
            } else if (view == View.EDITCONFIGURATION) {
                editConf = new EditConfiguration(this);
                hp.add(editConf);
            }

            RootPanel.get("content").add(hp);
        }
    }

    public enum View {
        LOGIN,
        LOGMODULES,
        DISPLAYLIST,
        EDITCONFIGURATION
    }
}
