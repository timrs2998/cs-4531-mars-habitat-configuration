package racoons.client;

import backend.common.Configuration;
import backend.common.Module;
import backend.common.Status;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * A class for editing configurations shown on a map.
 *
 * @author Tim
 *
 */
public class EditConfiguration extends Composite {

    private final transient VerticalPanel verticalPanel = new VerticalPanel();
    private final transient Label title = new Label("Edit Saved Configurations");

    // Viewing saved configurations
    private final transient Label saved_title = new Label("Saved configurations depicted in a list");
    private final transient FlexTable table_saved = new FlexTable();
    private transient List<Configuration> saved_list = new LinkedList<Configuration>();

    private final transient Button createNew = new Button("Create new configuration from usable logged modules");
    private final transient Button saveConfig = new Button("Save the current configuration");
    private final transient Label editHelp = new Label("To move a module, "
            + "click it to select it and click "
            + "on the desired location. Press save to keep changes.");

    private Configuration currentConfiguration;

    private final MarsMap map = new MarsMap(true);

    private final Context context;

    /**
     * Constructor
     *
     * @param context
     */
    public EditConfiguration(Context context) {
        super();
        this.context = context;

        title.addStyleName("editHeader");
        verticalPanel.add(title);
        verticalPanel.add(saved_title);
        verticalPanel.add(table_saved);
        verticalPanel.add(createNew);
        verticalPanel.add(saveConfig);
        verticalPanel.add(editHelp);
        verticalPanel.add(map);

        saveConfig.setEnabled(false);
        saveConfig.addClickHandler(new SaveConfig());

        createNew.addClickHandler(new CreateConfig());

        // Don't let user edit with no logged modules
        if (context.core.getModules().size() <= 0) createNew.setEnabled(false);

        table_saved.addClickHandler(new SavedTableClickHandler());
        update_saved();

        // Housekeeping
        initWidget(verticalPanel);
        this.addStyleName("editConfiguration");
    }

    private class CreateConfig implements ClickHandler {
        @Override
        public void onClick(ClickEvent event) {
            assert context.core.getModules().size() > 0;

            // Get logged modules, removing unusable modules
            List<Module> modules = context.core.getModules();
            for (Iterator<Module> iter = modules.iterator(); iter.hasNext(); ) {
                Module module = iter.next();

                if (module.getStatus() != Status.UNDAMAGED) {
                    iter.remove();
                }
            }

            // Make a configuration from usable modules
            currentConfiguration = new Configuration(modules);

            // Enable the save button
            saveConfig.setEnabled(true);

            // Clear the map of any modules/lines
            map.clearMap();

            // Change the map displayed
            map.drawConfiguration(currentConfiguration, MarsMap.getConfigurationOptions());
        }
    }

    private class SaveConfig implements ClickHandler {
        @Override
        public void onClick(ClickEvent event) {
            assert currentConfiguration != null;
            // Try to remove a configuration that matches currentConfiguration
            context.core.removeSavedConfiguration(currentConfiguration);

            // Add currentConfiguration to local storage
            context.core.saveConfiguration(new Configuration(map.getModulesAfterEdits()));

            // Update the currentConfiguration to allow for more saves
            currentConfiguration = new Configuration(map.getModulesAfterEdits());

            // Update the table of saved configurations
            update_saved();
        }
    }

    private final void update_saved() {
        table_saved.clear();

        saved_list = context.core.getSavedConfigurations();

        // Initialize header row in table
        table_saved.setText(0, 0, "Quality metric");
        table_saved.setText(0, 1, "Code numbers and position coords of modules (code, x, y, z)");

        // Add all elements in the list to the table
        for (int i = 1; i <= saved_list.size(); i++) {
            table_saved.setText(i, 0, saved_list.get(i - 1).getConformanceToRules() * 10 + "");
            table_saved.setText(i, 1, saved_list.get(i - 1).toString());
        }

        table_saved.getRowFormatter().addStyleName(0, "listTableHead");
        for (int i = 1; i <= saved_list.size(); i++) {
            table_saved.getRowFormatter().setStyleName(i, "listTableRow");
        }
    }

    private class SavedTableClickHandler implements ClickHandler {

        @Override
        public void onClick(final ClickEvent event) {
            try {
                final int row = table_saved.getCellForEvent(event).getRowIndex();

                // Ensure that title row wasn't clicked
                if (row != 0) {
                    currentConfiguration = saved_list.get(row - 1);

                    // Enable the save button
                    saveConfig.setEnabled(true);

                    // Clear the map of any modules/lines
                    map.clearMap();

                    // Change the map displayed
                    map.drawConfiguration(currentConfiguration, MarsMap.getConfigurationOptions());
                }
            } catch (final Exception e) {
            }
        }
    }
}
