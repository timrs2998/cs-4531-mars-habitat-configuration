package racoons.client;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * @author Tim and James
 */
public class DateWidget extends Composite {
    public DateWidget(final Context context) {
        // double first = Double.parseDouble(context.core.getFirstRunDate());
        // double millisElapsed = System.currentTimeMillis() - first;
        //
        // double days = millisElapsed * 1.157 * Math.pow(10, -8);
        //
        // if (days >= 10) {
        // Window.alert("10 days have elapsed");
        // }
        //
        // Label myLabel = new Label(days + " days elapsed since first run");
        //
        // initWidget(myLabel);
        final VerticalPanel vp = new VerticalPanel();
        Button resetButton = new Button("Reset Rover Time");
        double first = Double.parseDouble(context.core.getFirstRunDate());
        double millisElapsed = System.currentTimeMillis() - first;
        double days = Math.round(millisElapsed * 1.157 * Math.pow(10, -8));
        if (days >= 10) {
            Window.alert("10 days have elapsed");
        }
        final Label myLabel = new Label(days + " days elapsed since last rover charge");
        vp.add(myLabel);
        vp.add(resetButton);
        initWidget(vp);
        resetButton.addClickHandler(new ClickHandler() {
            @Override
            public void onClick(final ClickEvent event) {
                context.core.resetDate();
                // double first =
                // Double.parseDouble(context.core.getFirstRunDate());
                // double millisElapsed = System.currentTimeMillis() - first;
                double days = 0;
                myLabel.setText(days + " days elapsed since last rover charge");
            }
        });
    }
}
