## Mars Habitat Configuration Software ##

```bash
$ ./gradlew createPublic
$ open public/index.html
```

Note that index.html contains the Google Maps API Key:
* https://console.cloud.google.com/google/maps-apis/home;onboard=true?project=cs-4531-mars-habitat

The project may not work locally due to restrictions on the referrer configured in the GCP Console.

If the map does not load, disable ublock origin. Ublock origin causes the browser to block cross-origin requests.