object Versions {

    const val ktfmt = "0.46"
    const val palantirJavaFormat = "2.42.0"
    const val gradle = "8.7"
    const val gwt = "2.10.0"
    const val gwtGradlePlugin = "1.1.30"
    const val junit = "5.10.2"
    const val spotlessGradle = "6.25.0"

}
