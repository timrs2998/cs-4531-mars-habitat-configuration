package backend.common;

/**
 * Enum used to represent the type of module.
 *
 * @author Tim
 */
public enum Type {
    PLAIN,
    AIRLOCK,
    MEDICAL,
    POWER,
    CONTROL,
    CANTEEN,
    FOODWATER,
    SANITATION,
    GYMRELAX,
    DORMITORY,
    UNKNOWN;

    /**
     * Given a module code, return what type the modules is.
     *
     * @param code
     *            a module's code number
     * @return the type of module
     */
    public static Type getType(final int code) {
        Type type;

        if (code >= 1 && code <= 40) {
            type = PLAIN;
        } else if (code >= 61 && code <= 80) {
            type = DORMITORY;
        } else if (code >= 91 && code <= 100) {
            type = SANITATION;
        } else if (code >= 111 && code <= 120) {
            type = FOODWATER;
        } else if (code >= 131 && code <= 134) {
            type = GYMRELAX;
        } else if (code >= 141 && code <= 144) {
            type = CANTEEN;
        } else if (code >= 151 && code <= 154) {
            type = POWER;
        } else if (code >= 161 && code <= 164) {
            type = CONTROL;
        } else if (code >= 171 && code <= 174) {
            type = AIRLOCK;
        } else if (code >= 181 && code <= 184) {
            type = MEDICAL;
        } else {
            type = UNKNOWN;
        }

        return type;
    }

    @Override
    public String toString() {
        String type;

        if (this == PLAIN) {
            type = "plain";
        } else if (this == DORMITORY) {
            type = "dormitory";
        } else if (this == SANITATION) {
            type = "sanitation";
        } else if (this == FOODWATER) {
            type = "foodwater";
        } else if (this == GYMRELAX) {
            type = "gymrelax";
        } else if (this == CANTEEN) {
            type = "canteen";
        } else if (this == POWER) {
            type = "power";
        } else if (this == AIRLOCK) {
            type = "airlock";
        } else if (this == MEDICAL) {
            type = "medical";
        } else if (this == CONTROL) {
            type = "control";
        } else {
            type = "unknown";
        }

        return type;
    }
}
