package backend.common;

import backend.calculate.configurations.Habitat;
import backend.calculate.configurations.Min;
import backend.calculate.path.Calculation;
import com.google.gwt.json.client.JSONArray;
import com.google.gwt.json.client.JSONParser;
import com.google.gwt.json.client.JSONString;
import com.google.gwt.storage.client.Storage;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * The public API used by the GUI.
 *
 * @author Tim
 */
public final class Core {
    private final List<Module> modules = new LinkedList<Module>();
    private String firstRunDate;
    private final Storage storage;

    /**
     * The sole constructor. Initializes all previously logged modules from
     * local storage and makes note of when the user first ran the software.
     */
    public Core() {
        // Get the local storage object, assume it is supported
        this.storage = Storage.getLocalStorageIfSupported();
        assert this.storage != null;

        // Get the first run date
        firstRunDate = null;
        if (this.storage.getItem("date") == null || this.storage.getItem("date").equals("")) {
            firstRunDate = System.currentTimeMillis() + "";
            this.storage.setItem("date", firstRunDate);
        } else {
            firstRunDate = this.storage.getItem("date");
        }

        // Restore logged module from localstorage
        for (int i = 0; i < storage.getLength(); i++) {
            String key = storage.key(i);
            if (key.contains("module")) {
                String modString = storage.getItem(key);

                String[] components = modString.split(" ");
                int code = Integer.parseInt(components[0]);
                int[] position = new int[] {
                    Integer.parseInt(components[1]), Integer.parseInt(components[2]), Integer.parseInt(components[3])
                };
                Status status;
                if (components[4].equals("undamaged")) {
                    status = Status.UNDAMAGED;
                } else {
                    status = Status.DAMAGED;
                }
                int orientation = Integer.parseInt(components[5]);

                modules.add(new Module(code, position, status, orientation));
            }
        }
    }

    public void resetDate() {
        firstRunDate = System.currentTimeMillis() + "";
        storage.setItem("date", firstRunDate);
    }

    public String getFirstRunDate() {
        return firstRunDate;
    }

    /**
     * Simulates logging a module by adding it to a list, and storing it in
     * local storage.
     *
     * @param code
     *            the code of the module to log
     * @param pos
     *            an integer array containing the x, y, and z position
     *            coordinates
     * @param status
     *            the status of the module
     * @param orient
     *            the orientation (0, 1, or 2) of the module
     */
    public final void addModule(final int code, final int[] pos, final Status status, final int orient) {
        addModule(new Module(code, pos, status, orient));
    }

    /**
     * Simulates logging a module by adding it to a list, and storing it in
     * local storage.
     *
     * @param module
     *            the module to log
     */
    public final void addModule(final Module module) {
        modules.add(module);

        // Save the module to localstorage here
        storage.setItem(
                "module" + module.getCode(),
                module.getCode() + " "
                        + module.getPosition()[0] + " " + module.getPosition()[1] + " "
                        + 0 + " " + module.getStatus().toString().toLowerCase() + " "
                        + module.getOrientation());
    }

    /**
     * Called when the user wishes to see a list of all currently logged
     * modules.
     *
     * @return a list of all logged modules
     */
    public final List<Module> getModules() {
        final List<Module> outModules = new LinkedList<Module>();
        for (final Module m : modules) {
            outModules.add(m.clone());
        }
        return outModules;
    }

    /**
     * Calculates all of the configurations using the previously logged modules.
     *
     * @return a list of calculated configurations
     */
    public final List<Configuration> getConfigurations() {
        final List<Configuration> configs = new LinkedList<Configuration>();

        Habitat habitat = new Habitat(modules);
        List<List<Module>> habitatList = habitat.configureHabitats();

        while (!habitatList.isEmpty()) {
            configs.add(new Configuration(habitatList.remove(0)));
        }

        if (Min.getConfig(modules) != null && configs.isEmpty()) {
            configs.add(Min.getConfig(modules));
        }

        return configs;
    }

    /**
     * Called when the user wishes to save a particular configuration in local
     * storage.
     *
     * @param config
     */
    public final void saveConfiguration(final Configuration config) {
        // Create a JSONARRAY of modules belonging to this configuration
        JSONArray array = new JSONArray();
        for (int i = 0; i < config.getModules().size(); i++) {
            Module module = config.getModules().get(i);
            JSONString string = new JSONString(module.getCode() + " "
                    + module.getPosition()[0] + " " + module.getPosition()[1]
                    + " " + 0 + " "
                    + module.getStatus().toString().toLowerCase() + " "
                    + module.getOrientation());
            array.set(i, string);
        }

        // Determine a valid configuration name
        int index = 0;
        while (!"".equals(storage.getItem("config" + index)) && storage.getItem("config" + index) != null) {
            index++;
        }

        // Add the configuration to localstorage
        storage.setItem("config" + index, array.toString());
    }

    /**
     * Called when the user wishes to view all of the configurations he or she
     * has saved.
     *
     * @return a list of all saved configurations
     */
    public final List<Configuration> getSavedConfigurations() {
        final List<Configuration> configs = new LinkedList<Configuration>();
        int index = 0;
        while (!"".equals(storage.getItem("config" + index)) && storage.getItem("config" + index) != null) {
            JSONArray array = (JSONArray) JSONParser.parseLenient(storage.getItem("config" + index));

            // Begin constructing a list of modules to make the configuration
            List<Module> modules = new LinkedList<Module>();

            for (int i = 0; i < array.size(); i++) {
                // Parse out all of the modules details
                int code =
                        Integer.parseInt(array.get(i).toString().split(" ")[0].substring(1));
                int xPosition = Integer.parseInt(array.get(i).toString().split(" ")[1]);
                int yPosition = Integer.parseInt(array.get(i).toString().split(" ")[2]);
                int zPosition = Integer.parseInt(array.get(i).toString().split(" ")[3]);
                Status status;
                if (array.get(i).toString().split(" ")[4].equals("undamaged")) {
                    status = Status.UNDAMAGED;
                } else {
                    status = Status.DAMAGED;
                }
                int orientation =
                        Integer.parseInt(array.get(i).toString().split(" ")[5].substring(0, 1));

                // Construct the module, add it to list
                modules.add(new Module(code, new int[] {xPosition, yPosition, zPosition}, status, orientation));
            }

            // Construct the configuration, add it to list
            configs.add(new Configuration(modules));
            index++;
        }
        return configs;
    }

    /**
     * Called when the user wants to delete a particular saved configuration
     * from local storage.
     *
     * @param config
     *            the configuration to delete
     */
    public final void removeSavedConfiguration(final Configuration config) {
        // TODO: Eric, purge a given configuration
        // from local storage.

        // contains() is a useful method in List class,
        // as well as equals() in Configuration class.
        int index = 0;
        while (!"".equals(storage.getItem("config" + index)) && storage.getItem("config" + index) != null) {
            JSONArray array = (JSONArray) JSONParser.parseLenient(storage.getItem("config" + index));

            // Begin constructing a list of modules to make the configuration
            List<Module> modules = new LinkedList<Module>();

            for (int i = 0; i < array.size(); i++) {
                // Parse out all of the modules details
                int code =
                        Integer.parseInt(array.get(i).toString().split(" ")[0].substring(1));
                int xPosition = Integer.parseInt(array.get(i).toString().split(" ")[1]);
                int yPosition = Integer.parseInt(array.get(i).toString().split(" ")[2]);
                int zPosition = Integer.parseInt(array.get(i).toString().split(" ")[3]);
                Status status;
                if (array.get(i).toString().split(" ")[4].equals("undamaged")) {
                    status = Status.UNDAMAGED;
                } else {
                    status = Status.DAMAGED;
                }
                int orientation =
                        Integer.parseInt(array.get(i).toString().split(" ")[5].substring(0, 1));

                // Construct the module, add it to list
                modules.add(new Module(code, new int[] {xPosition, yPosition, zPosition}, status, orientation));
            }

            // Remove configuration from localstorage if it matches
            if (config.equals(new Configuration(modules))) {
                storage.removeItem("config" + index);
            }
            index++;
        }
    }

    /**
     * Called when the user wishes to delete a particular saved module from
     * local storage.
     *
     * @param m
     *            the module to delete
     */
    public final void removeSavedModule(final Module m) {
        modules.remove(m);
        storage.removeItem("module" + m.getCode());
    }

    /**
     * Returns the date recorded from the constructor.
     *
     * @return the date the user first used the system
     */
    public final Date getInitialDate() {
        return new Date(System.currentTimeMillis());
    }

    /**
     * Using the logged modules and the given configuration, calculates the
     * ideal location for the configuration for the Minimum Resource Solution.
     *
     * @param config
     *            the configuration to reposition
     * @return the configuration with all modules shifted
     */
    public final Configuration getBestLocation(final Configuration config) {
        // Make sure to use modules to know their original location
        // TODO: finish me
        // final List<Module> modules = new LinkedList<Module>();
        // for (final Module m : config.getModules()) {
        // final int x = m.getPosition()[0] + 20;
        // final int y = m.getPosition()[1] + 20;
        //
        // final Module mod = m.clone();
        // mod.setXYZPositions(x, y, mod.getPosition()[2]);
        //
        // modules.add(mod);
        // }
        return config;
    }

    /**
     * Using the logged modules and the given configuration, calculates the best
     * path for the rover to take in constructing the configuration.
     *
     * @param config
     *            the configuration to construct
     * @return a list of positions for the rover to travel to
     */
    public final List<int[]> getBestRoverPath(final Configuration config) {
        List<Module> mappedModule = config.getModules();
        List<List<Integer>> pathTaken = new ArrayList<List<Integer>>();
        List<Integer> startLoc = new ArrayList<Integer>();
        List<int[]> path = new LinkedList<int[]>();
        int y;
        int x;

        startLoc.add(25);
        startLoc.add(50);

        Calculation calculate = new Calculation(mappedModule, modules, startLoc);
        pathTaken = calculate.calculatePath();

        for (List<Integer> i : pathTaken) {
            y = i.get(0);
            x = i.get(1);

            path.add(new int[] {x, y, 0});
        }

        return path;
    }
}
