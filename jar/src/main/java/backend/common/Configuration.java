package backend.common;

import static backend.common.Type.AIRLOCK;
import static backend.common.Type.CANTEEN;
import static backend.common.Type.CONTROL;
import static backend.common.Type.DORMITORY;
import static backend.common.Type.FOODWATER;
import static backend.common.Type.GYMRELAX;
import static backend.common.Type.MEDICAL;
import static backend.common.Type.PLAIN;
import static backend.common.Type.POWER;
import static backend.common.Type.SANITATION;

import java.util.LinkedList;
import java.util.List;

public class Configuration {
    /**
     * The list of modules that make up this configuration.
     */
    private final List<Module> modules;

    /**
     * The list of rules that a configuration should follow.
     */
    private final List<Rule> rulesViolatable;

    private final List<Rule> rulesNonViolatable;

    /**
     * Initialize a configuration with a list of modules.
     *
     * @param modules
     *            a list of modules that form a Configuration
     */
    public Configuration(final List<Module> modulesIn) {
        modules = new LinkedList<Module>();
        for (final Module m : modulesIn) {
            modules.add(m.clone());
        }

        rulesViolatable = new LinkedList<Rule>();
        rulesViolatable.add(new Rule1());
        rulesViolatable.add(new Rule2());
        rulesViolatable.add(new Rule3());
        rulesViolatable.add(new Rule5());
        rulesViolatable.add(new Rule8());
        rulesViolatable.add(new Rule9());
        rulesViolatable.add(new Rule12());

        rulesNonViolatable = new LinkedList<Rule>();
        rulesNonViolatable.add(new IsConnected());
        rulesNonViolatable.add(new NoDuplicates());
        rulesNonViolatable.add(new NoOverlaps());
        rulesNonViolatable.add(new RuleAirlockPlain());
    }

    @Override
    public final String toString() {
        String s = "";
        for (final Module m : modules) {
            s += m.toString() + " ";
        }
        return s;
    }

    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (modules == null ? 0 : modules.hashCode());
        return result;
    }

    /**
     * An interface used to create a rule. Alls rules are either followed or
     * violated, and can return a string explaining what it means for the rule
     * to be violated.
     *
     * @author Tim
     *
     */
    public interface Rule {
        /**
         * Checks whether the rule is followed or not.
         *
         * @return true if the rule is followed, false if violated
         */
        public boolean isFollowed(List<Module> modules);

        /**
         * Returns a statement explaining what it means for the rule to be
         * violated.
         *
         * @return String
         */
        @Override
        public String toString();
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Configuration other = (Configuration) obj;
        if (modules == null) {
            if (other.modules != null) {
                return false;
            }
        } else if (!modules.equals(other.modules)) {
            return false;
        }
        return true;
    }

    public List<Rule> getViolatedRules() {
        final List<Rule> rulesViolated = new LinkedList<Rule>();

        for (final Rule rule : rulesNonViolatable) {
            if (!rule.isFollowed(modules)) {
                rulesViolated.add(rule);
            }
        }

        for (final Rule rule : rulesViolatable) {
            if (!rule.isFollowed(modules)) {
                rulesViolated.add(rule);
            }
        }
        return rulesViolated;
    }

    /**
     * Returns the modules making up this configuration.
     *
     * @return a list of modules that make up the configuration
     */
    public final List<Module> getModules() {
        final List<Module> outModules = new LinkedList<Module>();
        for (final Module m : modules) {
            outModules.add(m.clone());
        }
        return outModules;
    }

    /**
     * Returns a percentage (scaled between 0 and 1) of how many rules were
     * followed. If it is not a valid configuration then 0 is returned. Can be
     * used as a heuristic.
     *
     * @return double between 0 and 1
     */
    public final double getConformanceToRules() {
        final int NUMRULES = 7;
        int count = 0;

        for (final Rule rule : rulesNonViolatable) {
            if (!rule.isFollowed(modules)) {
                return 0;
            }
        }

        for (final Rule rule : rulesViolatable) {
            if (rule.isFollowed(modules)) {
                count += 1;
            }
        }
        return (double) count / (double) NUMRULES;
    }

    /**
     * Check that the entire configuration is all connected.
     */
    public static class IsConnected implements Rule {
        @Override
        public boolean isFollowed(final List<Module> modules) {
            boolean check = true;
            for (final Module a : modules) {
                check = true;
                for (final Module b : modules) {
                    if (a != b && a.isNextToHorizontal(b)) {
                        check = false;
                    }
                }
                if (check) {
                    return false;
                }
            }
            return true;
        }

        @Override
        public String toString() {
            return "Not all modules are connected to another module";
        }
    }

    /**
     * Check that there are no duplicate modules.
     */
    public static class NoDuplicates implements Rule {

        @Override
        public boolean isFollowed(final List<Module> modules) {
            for (final Module a : modules) {
                for (final Module b : modules) {
                    if (a != b && a.getCode() == b.getCode()) {
                        return false;
                    }
                }
            }
            return true;
        }

        @Override
        public String toString() {
            return "There are two modules with the same code number.";
        }
    }

    /**
     * Check that there are no overlapping modules.
     */
    public static class NoOverlaps implements Rule {
        @Override
        public final boolean isFollowed(final List<Module> modules) {
            for (final Module a : modules) {
                for (final Module b : modules) {
                    if (a != b) {
                        if (a.getPosition()[0] == b.getPosition()[0]
                                && a.getPosition()[1] == b.getPosition()[1]
                                && a.getPosition()[2] == b.getPosition()[2]) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        @Override
        public String toString() {
            return "Two modules have the same position and overlap.";
        }
    }

    /**
     * Checks if Rule #1 is violated. Returns false if it is violated, true if
     * the rule holds.
     */
    public static class Rule1 implements Rule {
        @Override
        public boolean isFollowed(final List<Module> modules) {
            return allM_allN(modules, SANITATION, CANTEEN, new Check() {
                @Override
                public boolean doCheck(final Module a, final Module b) {
                    return !a.isNextTo(b);
                }
            });
        }

        @Override
        public String toString() {
            return "1) There is a sanitation module next to a canteen module.";
        }
    }

    /**
     * Checks if Rule #2 is violated. Returns false if it is violated, true if
     * the rule holds.
     */
    public static class Rule2 implements Rule {
        @Override
        public boolean isFollowed(final List<Module> modules) {
            return allM_allN(modules, SANITATION, FOODWATER, new Check() {
                @Override
                public boolean doCheck(final Module a, final Module b) {
                    return !a.isNextTo(b);
                }
            });
        }

        @Override
        public String toString() {
            return "2) There is a sanitation module next to a food and " + "water module.";
        }
    }

    /**
     * Checks if Rule #3 is violated. Returns false if it is violated, true if
     * the rule holds.
     */
    public static class Rule3 implements Rule {
        @Override
        public boolean isFollowed(final List<Module> modules) {
            return allM_allN(modules, AIRLOCK, DORMITORY, new Check() {
                @Override
                public boolean doCheck(final Module a, final Module b) {
                    return !a.isNextTo(b);
                }
            });
        }

        @Override
        public String toString() {
            return "3) There is an airlock module next to a dormitory module.";
        }
    }

    /**
     * TODO: Finish rules 4, 6, and 7
     */

    /**
     * Checks if Rule #5 is violated. Returns false if it is violated, true if
     * the rule holds.
     */
    public static class Rule5 implements Rule {
        @Override
        public boolean isFollowed(final List<Module> modules) {
            // For all Modules m
            for (final Module m : modules) {
                int count = 0;

                // Sum up the number of modules bordering m
                for (final Module n : modules) {
                    if (m != n && m.isNextToHorizontal(n)) {
                        count += 1;
                    }
                }

                // If 4 Modules border m, then it is landlocked
                if (count >= 4) {
                    return false;
                }
            }
            return true;
        }

        @Override
        public String toString() {
            return "5) There is at least one landlocked module.";
        }
    }

    /**
     * Checks if Rule #8 is violated. Returns false if it is violated, true if
     * the rule holds.
     */
    public static class Rule8 implements Rule {
        @Override
        public boolean isFollowed(final List<Module> modules) {
            return allMExistsN(modules, GYMRELAX, SANITATION, new Check() {
                @Override
                public boolean doCheck(final Module a, final Module b) {
                    return a.isNextToHorizontal(b);
                }
            });
        }

        @Override
        public String toString() {
            return "8) There is a gym and relaxation module next to a sanitation" + "module.";
        }
    }

    /**
     * Checks if Rule #9 is violated. Returns false if it is violated, true if
     * the rule holds.
     */
    public static class Rule9 implements Rule {
        @Override
        public boolean isFollowed(final List<Module> modules) {
            // Check that all Medicals are next to Airlocks
            // final boolean check1 = allMExistsN(modules, MEDICAL, AIRLOCK,
            // new Check() {
            // @Override
            // public boolean doCheck(final Module a, final Module b) {
            // return !a.isNextTo(b) && a.isNextTo(b);
            // }
            // });

            // Check that all Airlocks are next to Medicals
            final boolean check2 = allMExistsN(modules, AIRLOCK, MEDICAL, new Check() {
                @Override
                public boolean doCheck(final Module a, final Module b) {
                    return a.isNextTo(b);
                }
            });
            return check2;
        }

        @Override
        public String toString() {
            return "9) There is at least one airlock module without a medical " + "module next to it";
        }
    }

    /**
     * Checks whether all Airlocks are connected to Plain modules and returns
     * true if this holds.
     */
    public static class RuleAirlockPlain implements Rule {
        @Override
        public boolean isFollowed(final List<Module> modules) {
            return allMExistsN(modules, AIRLOCK, PLAIN, new Check() {
                @Override
                public boolean doCheck(final Module a, final Module b) {
                    return a.isNextToHorizontal(b);
                }
            });
        }

        @Override
        public String toString() {
            return "There is at least one airlock module that are not " + "connected to a plain module.";
        }
    }

    /**
     * TODO: Finish rules 10 and 11
     */

    /**
     * Checks if Rule #12 is violated. Returns false if it is violated, true if
     * the rule holds.
     */
    public static class Rule12 implements Rule {
        @Override
        public boolean isFollowed(final List<Module> modules) {
            // Array of all special module Types
            final Type[] specials = new Type[] {GYMRELAX, CANTEEN, POWER, CONTROL, AIRLOCK, MEDICAL};

            // For every special Module Type
            for (final Type t : specials) {

                // Check that no two specials of the same type are "next to"
                final boolean pass = allMExistsN(modules, t, t, new Check() {
                    @Override
                    public boolean doCheck(final Module a, final Module b) {
                        return !a.isNextToHorizontal(b);
                    }
                });

                // If two specials are next to, fail the rule
                if (!pass) {
                    return true;
                }
            }
            return false;
        }

        @Override
        public String toString() {
            return "12) There are at least two special modules that are adjacent.";
        }
    }

    /**
     * Determines whether the following statement is true: For all Modules of
     * type m, there exists a Modules of type n such that check.doCheck(m, n) is
     * true.
     *
     * @param m
     *            is the first Type
     * @param n
     *            is the second Type
     * @param check
     *            is a class implementing the Check interface
     * @return whether the statement is true or not
     */
    private static final boolean allMExistsN(
            final List<Module> modules, final Type m, final Type n, final Check check) {
        boolean truthValue;

        // For all Modules a of Type m
        for (final Module a : modules) {
            if (Type.getType(a.getCode()) == m) {
                truthValue = false;

                // For all Modules b of Type n
                for (final Module b : modules) {
                    if (Type.getType(b.getCode()) == n && a != b) {
                        // Does b make check.doCheck() true?
                        if (check.doCheck(a, b)) {
                            truthValue = true;
                        }
                    }
                }

                // If statement doesn't hold for m, it is false
                if (!truthValue) {
                    return false;
                }
            }
        }

        // We never found the statement false, so it must be true
        return true;
    }

    /**
     * Determines whether the following statement is true: For all Modules of
     * Type m, for all Modules of Type n, check.doCheck(m, n) is true.
     *
     * @param m
     *            is the first Type
     * @param n
     *            is the second Type
     * @param check
     *            is a class implementing the Check interface
     * @return whether the statement is true or not
     */
    private static final boolean allM_allN(final List<Module> modules, final Type m, final Type n, final Check check) {
        // For all Modules a of Type m
        for (final Module a : modules) {
            if (Type.getType(a.getCode()) == m) {

                // For all Mobules b of Type n
                for (final Module b : modules) {
                    if (Type.getType(b.getCode()) == n && a != b) {

                        // Does b make check.doCheck() false?
                        if (!check.doCheck(a, b)) {
                            return false;
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * An interface for doing a boolean operation against two modules.
     */
    private interface Check {
        boolean doCheck(final Module a, final Module b);
    }
}
