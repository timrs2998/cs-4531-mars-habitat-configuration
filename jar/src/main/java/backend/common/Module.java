package backend.common;

import java.util.Arrays;

/**
 * @author Tim
 */
public class Module {
    // Module details
    private int code;
    private int[] pos;
    private Status status;
    private int orient;

    /**
     * Prevent use of default constructor.
     */
    @SuppressWarnings("unused")
    private Module() {}

    /**
     * Sole constructor.
     *
     * @param code
     *            the modules code number
     * @param pos
     *            an integer array of the modules x, y, and z coordinates
     * @param status
     *            the usability status of the module
     * @param orient
     *            the numerical orientation (0, 1, or 2) of the module
     */
    public Module(final int inCode, final int[] inPos, final Status inStatus, final int inOrient) {
        code = inCode;
        pos = new int[3];
        System.arraycopy(inPos, 0, pos, 0, 3);
        status = inStatus;
        orient = inOrient;

        assert pos.length == 3;
        assert orient == 0 || orient == 1 || orient == 2;
    }

    public final Module clone() {
        final Module m = new Module(code, new int[] {pos[0], pos[1], pos[2]}, status, orient);
        assert m != this;

        return m;
    }

    public final int getCode() {
        return code;
    }

    public final void setCode(final int code) {
        this.code = code;
    }

    public final int[] getPosition() {
        return new int[] {pos[0], pos[1], pos[2]};
    }

    public final void setXYZPositions(final int x, final int y, final int z) {
        pos[0] = x;
        pos[1] = y;
        pos[2] = z;
    }

    public final void setXPosition(final int x) {
        pos[0] = x;
    }

    public final void setYPosition(final int y) {
        pos[1] = y;
    }

    public final void setZPosition(final int z) {
        pos[2] = z;
    }

    public final Status getStatus() {
        return status;
    }

    public final void setStatus(final Status status) {
        this.status = status;
    }

    public final int getOrientation() {
        return orient;
    }

    public final void setOrientation(final int orient) {
        this.orient = orient;
    }

    public final Type getType() {
        return Type.getType(code);
    }

    @Override
    public final String toString() {
        return "(" + code + ", " + pos[0] + ", " + pos[1] + ")";
    }

    @Override
    public final int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + code;
        result = prime * result + orient;
        result = prime * result + Arrays.hashCode(pos);
        result = prime * result + (status == null ? 0 : status.hashCode());
        return result;
    }

    @Override
    public final boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Module other = (Module) obj;
        if (code != other.code) {
            return false;
        }
        if (orient != other.orient) {
            return false;
        }
        if (!Arrays.equals(pos, other.pos)) {
            return false;
        }
        if (status != other.status) {
            return false;
        }
        return true;
    }

    /**
     * Returns whether or not this module is "next to" another module, where
     * "next to" means horizontally, vertically, and diagonally.
     *
     * @param other
     *            is the module to check "next to" with
     * @return whether this is "next to" other or not
     */
    public final boolean isNextTo(final Module other) {
        final int[] o_pos = other.getPosition();

        // Find the distance between the two positions
        final double distance = Math.sqrt(
                Math.pow(pos[0] - o_pos[0], 2) + Math.pow(pos[1] - o_pos[1], 2) + Math.pow(pos[2] - o_pos[2], 2));

        return distance < 2d;
    }

    /**
     * Returns whether or not this module is "next to" another module, where
     * "next to" is limited to horizontally next to.
     *
     * @param other
     *            is the module to check "next to horizontally" with
     * @return whether this is "next to horizonally" other or not
     */
    public final boolean isNextToHorizontal(final Module other) {
        final int[] o_pos = other.getPosition();

        // Find the distance between the two positions
        final double distance = Math.sqrt(
                Math.pow(pos[0] - o_pos[0], 2) + Math.pow(pos[1] - o_pos[1], 2) + Math.pow(pos[2] - o_pos[2], 2));

        return distance == 1d;
    }
}
