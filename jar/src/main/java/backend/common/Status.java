package backend.common;

/**
 * A public of all possible statuses a module can take on.
 *
 * @author Tim
 */
public enum Status {
    UNDAMAGED,
    DAMAGED
}
