package backend.calculate.configurations;

import backend.common.Module;
import backend.common.Status;
import backend.common.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * A class that configures a habitat base on the list of modules given.
 *
 * @author Chee Lee
 */
public class Habitat {
    /**
     * List of usable modules.
     */
    private List<Module> modules;

    /**
     * Habitat Constructor
     *
     * @param modules
     *            list of modules
     */
    public Habitat(final List<Module> modules) {
        this.modules = new ArrayList<Module>();

        for (Module module : modules) {
            if (module.getStatus() == Status.UNDAMAGED && !this.modules.contains(module)) {
                this.modules.add(module.clone());
            }
        }
    }

    /**
     * Configure both minimal and maximum configurations
     *
     * @return a list containing both minimal and maximum configurations
     */
    public final List<List<Module>> configureHabitats() {
        List<List<Module>> configList = new ArrayList<List<Module>>();
        List<List<Module>> min = new ArrayList<List<Module>>();
        List<List<Module>> max = new ArrayList<List<Module>>();

        min = configureMin();
        max = configureMax();

        while (!min.isEmpty()) {
            configList.add(min.remove(0));
        }

        while (max != null && !max.isEmpty()) {
            if (max.get(0).size() != 0) {
                configList.add(max.remove(0));
            } else {
                max.remove(0);
            }
        }

        return configList;
    }

    /**
     * Configures a minimal habitat configuration
     *
     * @return two lists of modules that makes up a minimal configuration, else
     *         empty list if minimal configuration is not possible
     */
    private List<List<Module>> configureMin() {
        List<List<Module>> configList = new ArrayList<List<Module>>();
        List<Module> minList = new ArrayList<Module>();
        List<Type> minTypes;
        State map;

        if (!isMinPossible()) {
            return configList;
        }

        minTypes = generateMinConfig();

        for (Type type : minTypes) {
            for (Module module : modules) {
                if (type == module.getType() && !minList.contains(module)) {
                    minList.add(module.clone());
                    break;
                }
            }
        }

        for (int i = 0; i < 2; i++) {
            map = new State(minList);
            configMinPlains(map, i);
            configMin(map);
            configList.add(copyModules(map.getMappedModules()));
        }

        return configList;
    }

    /**
     * Configures a maximum habitat configuration
     *
     * @return two lists of module that makes up a maximum configuration, else
     *         empty list if minimal configuration is not possible or if there
     *         are less than 4 plains
     */
    private List<List<Module>> configureMax() {
        List<List<Module>> list = new ArrayList<List<Module>>();
        State map;
        int plains = 0;
        int minMaxPlains = 4;

        for (Module module : modules) {
            if (module.getType().equals(Type.PLAIN)) {
                plains++;
            }
        }

        if (!isMinPossible() || plains < minMaxPlains) {
            return list;
        }

        for (int i = 0; i < 2; i++) {
            map = new State(modules);

            configMaxPlains(map, i);

            if (map != null) {
                configMin(map);
                search(map);

                list.add(copyModules(map.getMappedModules()));
            }
        }

        return list;
    }

    /**
     * Configure minimal plains placement
     *
     * @param map
     *            State to map modules onto
     * @param choice
     *            design choice
     */
    private void configMinPlains(State map, final int choice) {
        HabitatDesign habDesign = new HabitatDesign();

        if (choice == 0) {
            map = habDesign.getAirLockHorizontal(map);

        } else if (choice == 1) {
            map = habDesign.getAirLockVertical(map);
        }
    }

    /**
     * Configure maximum plains placement
     *
     * @param map
     *            State to map modules onto
     * @param choice
     *            design choice
     */
    private void configMaxPlains(State map, final int choice) {
        int airlocks = map.getNumberOfModules(Type.AIRLOCK);
        HabitatDesign habDesign = new HabitatDesign();

        if ((airlocks == 1 || airlocks == 2)) {
            if (choice == 0) {
                map = habDesign.getAirLockH(map);
            } else if (choice == 1) {
                map = habDesign.getAirLockCross(map);
            }
        } else if ((airlocks == 3 || airlocks == 4)) {
            if (choice == 0) {
                map = habDesign.getAirLockCross(map);
            } else if (choice == 1) {
                map = habDesign.getAirLockH(map);
            }
        }
    }

    /**
     * Configure minimal modules placement
     *
     * @param map
     *            State to map modules onto
     */
    private void configMin(final State map) {
        List<Type> minTypes = generateMinConfig();
        List<List<Integer>> availablePos = new ArrayList<List<Integer>>();
        List<Integer> pos = new ArrayList<Integer>();
        Type type = null;
        int j = 0;

        for (Module module : map.getMappedModules()) {
            Type t = module.getType();
            if (minTypes.contains(t)) {
                minTypes.remove(t);
            }
        }

        availablePos = getAvailableConnections(map);

        if (availablePos == null) {
            return;
        }

        while (!minTypes.isEmpty() && !availablePos.isEmpty()) {
            j = 0;

            while (j < availablePos.size()) {
                pos = availablePos.get(j);
                type = heuristic(minTypes, pos, map);

                if (type != null && map.setPosition(pos, type)) {
                    minTypes.remove(type);
                    availablePos.remove(pos);
                }
                j++;
            }
        }
    }

    /**
     * Method to search for possible modules placement given the constraints of
     * each modules
     *
     * @param map
     *            State to map modules onto
     */
    private void search(final State map) {
        List<List<Integer>> availablePos = getAvailableConnections(map);
        List<Type> typeList = new ArrayList<Type>();
        List<Integer> pos;
        Type type;

        availablePos = sortList(availablePos);

        while (!map.noModuleLeft() && !availablePos.isEmpty()) {
            pos = availablePos.remove(0);
            typeList = generatePossibleTypePlacement(pos, map);

            if (!typeList.isEmpty()) {
                type = heuristic(typeList, pos, map);
                if (type != null) {
                    map.setPosition(pos, type);
                    availablePos.remove(pos);
                } else {
                    availablePos.add(pos);
                }
            }
        }
    }

    /**
     * Try to add a position to a list of position
     *
     * @param position
     *            position to be added
     * @param posList
     *            list of positions
     * @param map
     *            State
     */
    @SuppressWarnings("unchecked")
    private void addConnection(final List<Integer> position, final ArrayList<List<Integer>> posList, final State map) {
        if (map.getTypeFromPos(position) == null
                && !posList.contains(position)
                && (map.thereExistHorizontalTo(Type.PLAIN, position, 1)
                        || map.thereExistVerticalTo(Type.PLAIN, position, 1))) {
            posList.add((List<Integer>) copyList(position));
        }
    }

    /**
     * Method to check if minimal configuration is possible
     *
     * @return true if minimal configuration is possible, else false
     */
    private boolean isMinPossible() {
        List<Type> minTypes = generateMinConfig();
        Module module;

        if (modules.isEmpty()) {
            return false;
        }

        for (Type type : minTypes) {
            for (int i = 0; i < modules.size(); i++) {
                module = modules.get(i);

                if (module.getType() == type) {
                    break;
                } else if (i == modules.size() - 1) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Method to copy a list of modules
     *
     * @param tar
     *            list of modules to be copied
     * @return a copy of tar
     */
    private List<Module> copyModules(final List<Module> tar) {
        List<Module> copyList = new ArrayList<Module>();

        for (Module module : tar) {
            copyList.add(module.clone());
        }

        return copyList;
    }

    /**
     * Method to return the list of available positions where modules can be
     * place onto
     *
     * @param map
     *            State to find possible positions
     * @return a list of of available positions
     */
    private ArrayList<List<Integer>> getAvailableConnections(final State map) {
        ArrayList<List<Integer>> availableConnections = new ArrayList<List<Integer>>();
        ArrayList<Integer> temp = new ArrayList<Integer>();
        int[] positions = new int[3];
        int y;
        int x;

        temp.add(0);
        temp.add(0);

        for (Module module : map.getMappedModules()) {
            positions = module.getPosition();
            y = positions[1];
            x = positions[0];

            if (module.getType() == Type.PLAIN) {
                for (int j = y - 1; j <= y + 1; j++) {
                    for (int k = x - 1; k <= x + 1; k++) {
                        temp.set(0, j);
                        temp.set(1, k);

                        addConnection(temp, availableConnections, map);
                    }
                }
            }
        }

        return availableConnections;
    }

    /**
     * Method to sort a list of modules by their positions where those who are
     * closer to the center to those who are further away
     *
     * @param list
     *            list of available positions
     * @return a sorted list
     */
    @SuppressWarnings("unchecked")
    private List<List<Integer>> sortList(final List<List<Integer>> list) {
        List<List<Integer>> tempList = (List<List<Integer>>) copyList(list);
        List<List<Integer>> sortList = new ArrayList<List<Integer>>();
        List<Integer> temp = new ArrayList<Integer>();
        List<Integer> temp2 = new ArrayList<Integer>();
        int y;
        int x;
        int y2;
        int x2;
        int centerX = 50;
        int centerY = 25;
        int sortClose = 0;
        int close = 0;
        int i = 0;

        if (list.isEmpty()) {
            return sortList;
        }

        temp = tempList.remove(0);
        sortList.add(temp);

        while (!tempList.isEmpty()) {
            i = 0;
            temp = tempList.remove(0);
            y = temp.get(0);
            x = temp.get(1);
            while (i < sortList.size()) {

                temp2 = sortList.get(i);
                y2 = temp2.get(0);
                x2 = temp2.get(1);

                sortClose = closeTo(centerX, centerY, x2, y2);
                close = closeTo(centerX, centerY, x, y);

                if (close <= sortClose) {
                    break;
                }
                i++;
            }
            sortList.add(i, temp);
        }

        return sortList;
    }

    /**
     * Method to generate a list of possible type placement given the current
     * position
     *
     * @param pos
     *            the current position
     * @param map
     *            State of the map
     * @return a list of Types on the current position
     */
    private List<Type> generatePossibleTypePlacement(final List<Integer> pos, final State map) {
        List<Type> possiblePlacement = new ArrayList<Type>();

        for (Type type : Type.values()) {
            if (map.getNumberOfModules(type) > 0 && checkPlacementRules(type, pos, map)) {
                possiblePlacement.add(type);
            }
        }
        return possiblePlacement;
    }

    /**
     * Method to generate the minimal amount of types to create a minimal
     * configuration
     *
     * @return a list of Types producing a minimal configuration
     */
    private List<Type> generateMinConfig() {
        List<Type> minConfig = new ArrayList<Type>();

        for (Type t : Type.values()) {
            if (t != Type.UNKNOWN && t != Type.GYMRELAX) {
                minConfig.add(t);
            }

            if (t == Type.PLAIN) {
                minConfig.add(t);
                minConfig.add(t);
            }
        }

        return minConfig;
    }

    /**
     * Method to copy any type of list
     *
     * @param tar
     *            target list to be copy
     * @return a copied list of tar
     */
    private List<?> copyList(final List<?> tar) {
        List<Object> copyList = new ArrayList<Object>();

        for (int i = 0; i < tar.size(); i++) {
            copyList.add(tar.get(i));
        }

        return copyList;
    }

    /**
     * Method to check the current position for the best possible type placement
     * onto the map
     *
     * @param typeList
     *            the list of types
     * @param positions
     *            the current position
     * @param map
     *            State
     * @return the best Type placement onto the current position
     */
    @SuppressWarnings("unchecked")
    private Type heuristic(final List<Type> typeList, final List<Integer> positions, final State map) {
        if (positions.isEmpty() || typeList.isEmpty()) {
            return null;
        }

        List<Type> copyTypeList = (List<Type>) copyList(typeList);
        List<Integer> pos = (List<Integer>) copyList(positions);

        Type hType = null;
        int hValue = -9;
        int tempValue;

        for (Type type : copyTypeList) {
            tempValue = heuristicValue(type, pos, map);

            if (hValue <= tempValue) {
                hValue = tempValue;
                hType = type;
            }
        }

        return hType;
    }

    /**
     * Method to return the heuristic value of the current type on the current
     * position of the map
     *
     * @param type
     *            current type
     * @param positions
     *            current position
     * @param map
     *            State
     * @return the value of the type on the current position
     */
    private int heuristicValue(final Type type, final List<Integer> positions, final State map) {
        int value = 0;
        int dormCount;
        int sanitationCount;

        if (type.equals(Type.PLAIN) || type.equals(Type.AIRLOCK)) {
            return value;

        } else if (type.equals(Type.POWER)) {
            if (map.thereExistAllSideTo(Type.CONTROL, positions, 1)) {
                return -2;
            }
        } else if (type.equals(Type.CONTROL)) {
            if (map.thereExistAllSideTo(Type.POWER, positions, 1)) {
                return -2;
            }
        } else if (type == Type.MEDICAL) {
            if (map.thereExistDiagonalTo(Type.AIRLOCK, positions, 1)
                    && !map.isModuleDiagonalDiagonalTo(Type.MEDICAL, Type.AIRLOCK, positions)) {
                value += 10;
            } else if (map.medicAirlockFull()) {
                value++;
            } else {
                value -= 2;
            }
        } else if (type == Type.CANTEEN) {
            if (!map.thereExistAllSideTo(Type.CANTEEN, positions, 3)) {
                value++;
            }
        } else if (type == Type.FOODWATER) {
            if (map.thereExistHorizontalTo(Type.CANTEEN, positions, 3)
                    || map.thereExistVerticalTo(Type.CANTEEN, positions, 3)) {
                value += 5;
            } else if (map.thereExistDiagonalTo(Type.CANTEEN, positions, 2)) {
                value += 5;
            } else {
                value--;
            }
        } else if (type == Type.DORMITORY) {
            dormCount = countNumberOfOccurence(positions, type, 2, map);
            if (dormCount % 2 != 0) {
                value++;
            }

            sanitationCount = countNumberOfOccurence(positions, Type.SANITATION, 2, map);
            if (sanitationCount != 0 && (dormCount / 2 != sanitationCount)) {
                value++;
            }
        } else if (type == Type.GYMRELAX) {
            if (map.thereExistHorizontalTo(Type.SANITATION, positions, 1)) {
                value += 5;
            } else if (map.thereExistVerticalTo(Type.PLAIN, positions, 1)
                    && !map.thereExistHorizontalTo(Type.GYMRELAX, positions, 1)) {
                value += 5;
            } else {
                value--;
            }
        } else if (type == Type.SANITATION) {
            if (map.thereExistAllSideTo(Type.CANTEEN, positions, 1)
                    || map.thereExistAllSideTo(Type.FOODWATER, positions, 1)) {
                return -5;
            }

            if (map.thereExistHorizontalTo(Type.GYMRELAX, positions, 1)
                    && !map.thereExistHorizontalTo(Type.SANITATION, positions, 2)) {
                return 6;
            }

            if (map.thereExistAllSideTo(Type.DORMITORY, positions, 1)) {
                value++;
            }
        }

        return value;
    }

    /**
     * Method to count the number of times a type occur on the map given the
     * current position
     *
     * @param positions
     *            the current position
     * @param type
     *            the type to check
     * @param numSpaces
     *            the number of spaces
     * @param map
     *            State
     * @return the number of times the type has occurred
     */
    @SuppressWarnings("unchecked")
    private int countNumberOfOccurence(
            final List<Integer> positions, final Type type, final int numSpaces, final State map) {
        List<Integer> pos = (List<Integer>) copyList(positions);
        int y = pos.get(0);
        int x = pos.get(1);
        int count = 0;

        for (int i = 1; i <= numSpaces; i++) {
            pos.set(0, y);
            pos.set(1, x - i);
            if (map.thereExistLHorizontalTo(type, pos, 1) && !pos.equals(positions)) {
                count++;
            }

            pos.set(0, y);
            pos.set(1, x + i);
            if (map.thereExistRHorizontalTo(type, pos, 1) && !pos.equals(positions)) {
                count++;
            }

            pos.set(0, y + i);
            pos.set(1, x);
            if (map.thereExistUpVerticalTo(type, pos, 1) && !pos.equals(positions)) {
                count++;
            }

            pos.set(0, y - i);
            pos.set(1, x);
            if (map.thereExistDownVerticalTo(type, pos, 1) && !pos.equals(positions)) {
                count++;
            }
        }
        return count;
    }

    private boolean checkPlacementRules(Type type, List<Integer> pos, State map) {
        if (type == Type.MEDICAL) {
            return map.thereExistDiagonalTo(Type.AIRLOCK, pos, 1) || map.medicAirlockFull();
        } else if (type == Type.CANTEEN) {
            return !map.thereExistAllSideTo(Type.SANITATION, pos, 1);
        } else if (type == Type.FOODWATER) {
            return !map.thereExistAllSideTo(Type.SANITATION, pos, 1);
        } else if (type == Type.GYMRELAX) {
            return map.thereExistVerticalTo(Type.PLAIN, pos, 1) && (map.thereExistDiagonalTo(Type.PLAIN, pos, 1));
        } else if (type == Type.DORMITORY) {
            return !map.thereExistAllSideTo(Type.AIRLOCK, pos, 1);
        } else if (type == Type.SANITATION) {
            return !(map.thereExistAllSideTo(Type.CANTEEN, pos, 1) || map.thereExistAllSideTo(Type.FOODWATER, pos, 1));
        } else if (type == Type.CONTROL || type == Type.POWER) {
            return true;
        }

        return false;
    }

    /**
     * Method to determine the closeness of x,y to the center
     *
     * @param centerX
     *            center x value
     * @param centerY
     *            center y value
     * @param x
     *            x value
     * @param y
     *            y value
     * @return value close to center
     */
    private int closeTo(final int centerX, final int centerY, final int x, final int y) {
        int sum = 0;

        if (x < centerX) {
            sum += centerX - x;
        } else {
            sum += x - centerX;
        }

        if (y < centerY) {
            sum += centerY - y;
        } else {
            sum += y - centerY;
        }

        return Math.abs(sum);
    }
}
