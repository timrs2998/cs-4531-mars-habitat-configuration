package backend.calculate.configurations;

import backend.common.Module;
import backend.common.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Class to design the plains of the map.
 * @author Chee Lee
 */
public class HabitatDesign {
    /**
     * max y value of the map
     */
    private final int MAXY = 50;

    /**
     * max x value of the map
     */
    private final int MAXX = 100;

    /**
     * random generator use to decide airlock position
     */
    private Random randGen;

    /**
     * Habitat Design Constructor
     */
    public HabitatDesign() {
        randGen = new Random();
    }

    /**
     * Generate a plain of a horizontal shape
     * @param map
     *            State
     * @return a map shape horizontal
     */
    public final State getAirLockHorizontal(State map) {
        int numPlains = map.getNumberOfModules(Type.PLAIN);
        int numAirlocks = map.getNumberOfModules(Type.AIRLOCK);
        int y = MAXY / 2;
        int x = MAXX / 2;
        int diff = numPlains / 2;

        List<Integer> startPos = new ArrayList<Integer>();
        List<Integer> tarPos = new ArrayList<Integer>();

        if (numAirlocks > 2 || numAirlocks <= 0 || numPlains < 3) {
            return null;
        }

        startPos.add(y);
        startPos.add(x - diff);

        tarPos.add(y);
        tarPos.add(x + diff);

        createHorizontal(startPos, tarPos, numPlains, map);

        figureAirlock(numPlains, numAirlocks, map);

        return map;
    }

    /**
     * Generate a plain of a Vertical shape
     * @param map
     *            State
     * @return a map shape Vertical
     */
    public State getAirLockVertical(State map) {
        int numPlains = map.getNumberOfModules(Type.PLAIN);
        int numAirlocks = map.getNumberOfModules(Type.AIRLOCK);
        int y = MAXY / 2;
        int x = MAXX / 2;
        int diff = numPlains / 2;

        List<Integer> startPos = new ArrayList<Integer>();
        List<Integer> tarPos = new ArrayList<Integer>();

        if (numAirlocks > 2 || numAirlocks <= 0 || numPlains < 3) {
            return null;
        }

        startPos.add(y - diff);
        startPos.add(x);

        tarPos.add(y + diff);
        tarPos.add(x);

        createVertical(startPos, tarPos, numPlains, map);

        figureAirlock(numPlains, numAirlocks, map);

        return map;
    }

    /**
     * Generate a plain of a "H" shape
     * @param map
     *            State
     * @return a map shape "H"
     */
    public State getAirLockH(State map) {
        int numPlains = map.getNumberOfModules(Type.PLAIN);
        int numAirlocks = map.getNumberOfModules(Type.AIRLOCK);
        int y = MAXY / 2;
        int x = MAXX / 2;
        int split = 3;

        int vertDiff;
        int horDiff;
        List<Integer> startPos = new ArrayList<Integer>();
        List<Integer> tarPos = new ArrayList<Integer>();

        if (numPlains < 3) {
            return null;
        } else if (numPlains < 4 && numAirlocks > 2) {
            numAirlocks = 2;
        }

        if (numPlains % split == 0) {
            vertDiff = numPlains / split;
            horDiff = numPlains / split;
        } else {
            vertDiff = numPlains / split;
            horDiff = numPlains / split + numPlains % split;
        }

        startPos.add(y);
        startPos.add(x - horDiff / 2);

        tarPos.add(y);
        tarPos.add(x + horDiff / 2 + horDiff % 2);

        createHorizontal(startPos, tarPos, horDiff, map);

        startPos.set(0, y - (vertDiff / 2));
        startPos.set(1, x - horDiff / 2 - horDiff % 2);

        tarPos.set(0, y + (vertDiff / 2) + vertDiff % 2);
        tarPos.set(1, x - horDiff / 2 - horDiff % 2);

        createVertical(startPos, tarPos, vertDiff, map);

        startPos.set(0, y - vertDiff / 2);
        startPos.set(1, x + horDiff / 2 + horDiff % 2);

        tarPos.set(0, y + vertDiff / 2 + vertDiff % 2);
        tarPos.set(1, x + horDiff / 2 + horDiff % 2);

        createVertical(startPos, tarPos, vertDiff, map);

        figureAirlock2(numPlains, numAirlocks, map);
        return map;
    }

    /**
     * Generate a plain of a "+" shape
     * @param map
     *            State
     * @return a map shape "+"
     */
    public State getAirLockCross(State map) {
        int numPlains = map.getNumberOfModules(Type.PLAIN);
        int numAirlocks = map.getNumberOfModules(Type.AIRLOCK);
        int split = 2;
        int y = MAXY / split;
        int x = MAXX / split;
        int xDiff = numPlains / split + numPlains % split;
        int yDiff = numPlains / split;

        List<Integer> startPos = new ArrayList<Integer>();
        List<Integer> tarPos = new ArrayList<Integer>();

        if (numPlains < 4) {
            return null;
        } else if (numPlains < 6 && numAirlocks > 2) {
            numAirlocks = 2;
        }

        startPos.add(y);
        startPos.add(x - xDiff / 2);

        tarPos.add(y);
        tarPos.add(x + xDiff / 2 + xDiff % 2);

        createHorizontal(startPos, tarPos, numPlains / 2 + numPlains % 2, map);

        startPos.set(0, y - yDiff / 2);
        startPos.set(1, x);

        tarPos.set(0, y + yDiff / 2 + yDiff % 2);
        tarPos.set(1, x);

        createVertical(startPos, tarPos, numPlains / 2, map);

        figureAirlock(numPlains, numAirlocks, map);

        return map;
    }

    /**
     * Generate plains in horizontal from
     * position start to position tar from left to right.
     * @param start
     *            start position
     * @param tar
     *            target position
     * @param plains
     *            number of plains
     * @param map
     *            State to map onto
     */
    private void createHorizontal(final List<Integer> start, final List<Integer> tar, final int plains, State map) {
        List<Integer> pos = new ArrayList<Integer>();
        int startY = start.get(0);
        int startX = start.get(1);
        int tarY = tar.get(0);
        int tarX = tar.get(1);
        int numPlains = plains;
        int i = 0;

        if (startX >= tarX || startY != tarY) {
            return;
        }

        pos.add(startY);
        pos.add(startX);

        while (numPlains >= 1 && startX + i <= tarX) {
            if (map.setPosition(pos, Type.PLAIN)) {
                numPlains--;
            } else if (tarX < MAXX) {
                tarX++;
            }

            i++;
            pos.set(1, startX + i);
        }
    }

    /**
     * Generate plains in Vertical from position start to position tar from
     * bottom to top
     * @param start
     *            start position
     * @param tar
     *            target position
     * @param plains
     *            number of plains
     * @param map
     *            State to map onto
     */
    private void createVertical(final List<Integer> start, final List<Integer> tar, final int plains, State map) {
        List<Integer> pos = new ArrayList<Integer>();
        int startY = start.get(0);
        int startX = start.get(1);
        int tarY = tar.get(0);
        int tarX = tar.get(1);
        int numPlains = plains;
        int i = 0;

        pos.add(startY);
        pos.add(startX);

        if (startX != tarX || startY >= tarY) {
            return;
        }

        while (numPlains >= 1 && startY + i <= tarY) {
            if (map.setPosition(pos, Type.PLAIN)) {
                numPlains--;
            } else if (tarY < MAXY) {
                tarY++;
            }

            i++;
            pos.set(0, startY + i);
        }
    }

    /**
     * figure airlock position for horizontal, vertical and "+" shapes
     * @param plains
     *            number of plains
     * @param airlocks
     *            number of airlocks
     * @param map
     *            State to map onto
     */
    private void figureAirlock(final int plains, final int airlocks, State map) {
        List<List<Integer>> possiblePos = new ArrayList<List<Integer>>();
        List<Integer> rightAirlockPos = new ArrayList<Integer>();
        List<Integer> leftAirlockPos = new ArrayList<Integer>();
        List<Integer> upAirlockPos = new ArrayList<Integer>();
        List<Integer> downAirlockPos = new ArrayList<Integer>();
        List<Integer> airlockPos = new ArrayList<Integer>();

        int numAirlocks = airlocks;
        int rand;

        rightAirlockPos = findRightMostPosition(map);
        leftAirlockPos = findLeftMostPosition(map);
        upAirlockPos = findTopMostPosition(map);
        downAirlockPos = findDownMostPosition(map);

        if (!rightAirlockPos.isEmpty() && rightAirlockPos.get(0) != 0) {
            rightAirlockPos.set(1, rightAirlockPos.get(1) + 1);
            leftAirlockPos.set(1, leftAirlockPos.get(1) - 1);

            possiblePos.add(rightAirlockPos);
            possiblePos.add(leftAirlockPos);
        }

        if (!upAirlockPos.isEmpty() && upAirlockPos.get(0) != 0) {
            upAirlockPos.set(0, upAirlockPos.get(0) + 1);
            downAirlockPos.set(0, downAirlockPos.get(0) - 1);

            possiblePos.add(upAirlockPos);
            possiblePos.add(downAirlockPos);
        }

        while (numAirlocks > 0 && !possiblePos.isEmpty()) {
            rand = randGen.nextInt(possiblePos.size());
            airlockPos = possiblePos.get(rand);
            if (!airlockPos.isEmpty() && map.setPosition(airlockPos, Type.AIRLOCK)) {
                numAirlocks--;
            }
            possiblePos.remove(airlockPos);
        }
    }

    /**
     * figure airlock position for "H" shape
     * @param plains
     *            number of plains
     * @param airlocks
     *            number of airlocks
     * @param map
     *            State to map onto
     */
    private void figureAirlock2(final int plains, final int airlocks, State map) {
        List<List<Integer>> possiblePos = new ArrayList<List<Integer>>();
        List<Integer> topRightAirlockPos = new ArrayList<Integer>();
        List<Integer> topLeftAirlockPos = new ArrayList<Integer>();
        List<Integer> bottomRightAirlockPos = new ArrayList<Integer>();
        List<Integer> bottomLeftAirlockPos = new ArrayList<Integer>();
        List<Integer> airlockPos = new ArrayList<Integer>();

        int numAirlocks = airlocks;
        int rand;

        topRightAirlockPos = findTopRightPosition(map);
        topLeftAirlockPos = findTopLeftPosition(map);
        bottomRightAirlockPos = findBottomRightPosition(map);
        bottomLeftAirlockPos = findBottomLeftPosition(map);

        if (topRightAirlockPos.get(0) != 100) {
            topRightAirlockPos.set(0, topRightAirlockPos.get(0) - 1);
            topLeftAirlockPos.set(0, topLeftAirlockPos.get(0) - 1);

            possiblePos.add(topLeftAirlockPos);
            possiblePos.add(topRightAirlockPos);
        }

        if (bottomRightAirlockPos.get(0) != 0) {
            bottomRightAirlockPos.set(0, bottomRightAirlockPos.get(0) + 1);
            bottomLeftAirlockPos.set(0, bottomLeftAirlockPos.get(0) + 1);

            possiblePos.add(bottomRightAirlockPos);
            possiblePos.add(bottomLeftAirlockPos);
        }

        while (numAirlocks > 0 && !possiblePos.isEmpty()) {
            rand = randGen.nextInt(possiblePos.size());
            airlockPos = possiblePos.get(rand);

            if (!airlockPos.isEmpty() && map.setPosition(airlockPos, Type.AIRLOCK)) {
                numAirlocks--;
            }
            possiblePos.remove(airlockPos);
        }
    }

    /**
     * Find the right most position on the map
     * @param map
     *            State to check onto
     * @return the right most position
     */
    private List<Integer> findRightMostPosition(State map) {
        List<Module> mappedPos = map.getMappedModules();
        List<Integer> rightMost = new ArrayList<Integer>();
        int[] pos = new int[3];
        int rightX = 0;
        int rightY = 0;
        int y;
        int x;

        for (Module module : mappedPos) {
            pos = module.getPosition();
            y = pos[1];
            x = pos[0];

            if (x > rightX && x != MAXX / 2) {
                rightX = x;
                rightY = y;
            }
        }
        rightMost.add(rightY);
        rightMost.add(rightX);

        return rightMost;
    }

    /**
     * Find the left most position on the map
     * @param map
     *            State to check onto
     * @return the left most position
     */
    private List<Integer> findLeftMostPosition(State map) {
        List<Module> mappedPos = map.getMappedModules();
        List<Integer> leftMost = new ArrayList<Integer>();
        int[] pos = new int[3];
        int leftX = 100;
        int leftY = 100;
        int y;
        int x;

        for (Module module : mappedPos) {
            pos = module.getPosition();

            y = pos[1];
            x = pos[0];

            if (x < leftX && x != MAXX / 2) {
                leftX = x;
                leftY = y;
            }
        }

        leftMost.add(leftY);
        leftMost.add(leftX);

        return leftMost;
    }

    /**
     * Find the top most position on the map
     * @param map
     *            State to check onto
     * @return the top most position
     */
    private List<Integer> findTopMostPosition(State map) {
        List<Module> mappedPos = map.getMappedModules();
        List<Integer> topMost = new ArrayList<Integer>();
        int[] pos = new int[3];
        int topX = 0;
        int topY = 0;
        int y;
        int x;

        for (Module module : mappedPos) {
            pos = module.getPosition();
            y = pos[1];
            x = pos[0];

            if (y > topY && y != MAXY / 2) {
                topX = x;
                topY = y;
            }
        }

        topMost.add(topY);
        topMost.add(topX);

        return topMost;
    }

    /**
     * Find the down most position on the map
     * @param map
     *            State to check onto
     * @return the down most position
     */
    private List<Integer> findDownMostPosition(State map) {
        List<Module> mappedPos = map.getMappedModules();
        List<Integer> downMost = new ArrayList<Integer>();
        int[] pos = new int[3];
        int downX = 100;
        int downY = 100;
        int y;
        int x;

        for (Module module : mappedPos) {
            pos = module.getPosition();
            y = pos[1];
            x = pos[0];

            if (y < downY && y != MAXY / 2) {
                downX = x;
                downY = y;
            }
        }

        downMost.add(downY);
        downMost.add(downX);

        return downMost;
    }

    /**
     * Find the top right most position on the map
     * @param map
     *            State to check onto
     * @return the top right most position
     */
    private List<Integer> findTopRightPosition(State map) {
        List<Module> mappedPos = map.getMappedModules();
        List<Integer> topRightMost = new ArrayList<Integer>();
        int[] pos = new int[3];
        int topX = 0;
        int topY = 100;
        int y;
        int x;

        for (Module module : mappedPos) {
            pos = module.getPosition();
            y = pos[1];
            x = pos[0];

            if (x > MAXX / 2 && x >= topX && y < topY) {
                topX = x;
                topY = y;
            }
        }

        topRightMost.add(topY);
        topRightMost.add(topX);

        return topRightMost;
    }

    /**
     * Find the top left most position on the map
     * @param map
     *            State to check onto
     * @return the top left most position
     */
    private List<Integer> findTopLeftPosition(State map) {
        List<Module> mappedPos = map.getMappedModules();
        List<Integer> topLeftMost = new ArrayList<Integer>();
        int[] pos = new int[3];
        int topX = 100;
        int topY = 100;
        int y;
        int x;

        for (Module module : mappedPos) {
            pos = module.getPosition();
            y = pos[1];
            x = pos[0];

            if (x < MAXX / 2 && x <= topX && y < topY) {
                topX = x;
                topY = y;
            }
        }

        topLeftMost.add(topY);
        topLeftMost.add(topX);

        return topLeftMost;
    }

    /**
     * Find the bottom right most position on the map
     * @param map
     *            State to check onto
     * @return the bottom right most position
     */
    private List<Integer> findBottomRightPosition(State map) {
        List<Module> mappedPos = map.getMappedModules();
        List<Integer> bottomRightMost = new ArrayList<Integer>();
        int[] pos = new int[3];
        int topX = 0;
        int topY = 0;
        int y;
        int x;

        for (Module module : mappedPos) {
            pos = module.getPosition();
            y = pos[1];
            x = pos[0];

            if (x > MAXX / 2 && x >= topX && y > topY) {
                topX = x;
                topY = y;
            }
        }

        bottomRightMost.add(topY);
        bottomRightMost.add(topX);

        return bottomRightMost;
    }

    /**
     * Find the bottom left most position on the map
     * @param map
     *            State to check onto
     * @return the bottom left most position
     */
    private List<Integer> findBottomLeftPosition(State map) {
        List<Module> mappedPos = map.getMappedModules();
        List<Integer> bottomLeftMost = new ArrayList<Integer>();
        int[] pos = new int[3];
        int topX = 100;
        int topY = 0;
        int y;
        int x;

        for (Module module : mappedPos) {
            pos = module.getPosition();
            y = pos[1];
            x = pos[0];

            if (x < MAXX / 2 && x <= topX && y > topY) {
                topX = x;
                topY = y;
            }
        }

        bottomLeftMost.add(topY);
        bottomLeftMost.add(topX);

        return bottomLeftMost;
    }
}
