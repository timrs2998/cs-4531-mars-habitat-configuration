package backend.calculate.configurations;

import backend.common.Configuration;
import backend.common.Module;
import backend.common.Status;
import backend.common.Type;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Tim
 */
public class Min {

    public static Configuration getConfig(List<Module> modules) {
        int countAirlock = 0;
        int countDorm = 0;
        int countSan = 0;
        int countFood = 0;
        int countCant = 0;
        int countPower = 0;
        int countControl = 0;
        int countPlain = 0;

        for (Module module : modules) {
            if (module.getType().equals(Type.AIRLOCK) && module.getStatus().equals(Status.UNDAMAGED)) countAirlock++;
            if (module.getType().equals(Type.DORMITORY) && module.getStatus().equals(Status.UNDAMAGED)) countDorm++;
            if (module.getType().equals(Type.SANITATION) && module.getStatus().equals(Status.UNDAMAGED)) countSan++;
            if (module.getType().equals(Type.FOODWATER) && module.getStatus().equals(Status.UNDAMAGED)) countFood++;
            if (module.getType().equals(Type.CANTEEN) && module.getStatus().equals(Status.UNDAMAGED)) countCant++;
            if (module.getType().equals(Type.POWER) && module.getStatus().equals(Status.UNDAMAGED)) countPower++;
            if (module.getType().equals(Type.CONTROL) && module.getStatus().equals(Status.UNDAMAGED)) countControl++;
            if (module.getType().equals(Type.PLAIN) && module.getStatus().equals(Status.UNDAMAGED)) countPlain++;
        }

        if (countPlain >= 3
                && countDorm >= 1
                && countSan >= 1
                && countFood >= 1
                && countCant >= 1
                && countPower >= 1
                && countControl >= 1
                && countAirlock >= 1) {
            // Make a temporary list
            List<Module> temM = new LinkedList<Module>();
            temM.addAll(modules);

            List<Module> config = new LinkedList<Module>();
            int plain = 0, dorm = 0, san = 0, fowat = 0, cant = 0, power = 0, control = 0, air = 0;
            for (Module m : modules) {
                if (m.getType().equals(Type.POWER) && power == 0) {
                    config.add(new Module(m.getCode(), new int[] {1, 2, 0}, Status.UNDAMAGED, 0));
                    power++;
                } else if (m.getType().equals(Type.CONTROL) && control == 0) {
                    config.add(new Module(m.getCode(), new int[] {2, 2, 0}, Status.UNDAMAGED, 0));
                    control++;
                } else if (m.getType().equals(Type.PLAIN) && plain == 0) {
                    config.add(new Module(m.getCode(), new int[] {3, 2, 0}, Status.UNDAMAGED, 0));
                    plain++;
                } else if (m.getType().equals(Type.PLAIN) && plain == 1) {
                    config.add(new Module(m.getCode(), new int[] {4, 2, 0}, Status.UNDAMAGED, 0));
                    plain++;
                } else if (m.getType().equals(Type.CANTEEN) && cant == 0) {
                    config.add(new Module(m.getCode(), new int[] {5, 2, 0}, Status.UNDAMAGED, 0));
                    cant++;
                } else if (m.getType().equals(Type.FOODWATER) && fowat == 0) {
                    config.add(new Module(m.getCode(), new int[] {6, 2, 0}, Status.UNDAMAGED, 0));
                    fowat++;
                } else if (m.getType().equals(Type.AIRLOCK) && air == 0) {
                    config.add(new Module(m.getCode(), new int[] {4, 1, 0}, Status.UNDAMAGED, 0));
                    air++;
                } else if (m.getType().equals(Type.PLAIN) && plain == 2) {
                    config.add(new Module(m.getCode(), new int[] {3, 3, 0}, Status.UNDAMAGED, 0));
                    plain++;
                } else if (m.getType().equals(Type.DORMITORY) && dorm == 0) {
                    config.add(new Module(m.getCode(), new int[] {4, 3, 0}, Status.UNDAMAGED, 0));
                    dorm++;
                } else if (m.getType().equals(Type.SANITATION) && san == 0) {
                    config.add(new Module(m.getCode(), new int[] {3, 4, 0}, Status.UNDAMAGED, 0));
                    san++;
                }
            }
            // TODO: center the configuration
            return new Configuration(config);
        }
        return null;
    }

    public static int[] getCenter(List<Module> modules) {
        return null;
    }
}
