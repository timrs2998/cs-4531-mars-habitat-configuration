package backend.calculate.configurations;

import backend.common.Module;
import backend.common.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * State class to store mapped modules.
 *
 * @author Chee Lee
 */
public class State {
    /**
     * Array of module to mapped modules
     */
    private Module[][] map;

    /**
     * list of available modules
     */
    private List<Module> modules;

    /**
     * list of mapped modules
     */
    private List<Module> mappedModules;

    /**
     * max y in array
     */
    private final int MAXY = 50;

    /**
     * max x in array
     */
    private final int MAXX = 100;

    /**
     * State constructor
     *
     * @param listModules
     *            list of modules
     */
    public State(final List<Module> listModules) {
        map = new Module[MAXY][MAXX];
        mappedModules = new ArrayList<Module>();
        this.modules = copyModules(listModules);
    }

    /**
     * Method to clone State
     *
     * @return a copied state
     */
    public final State clone() {
        State tempMap = new State(this.modules);
        tempMap.setMappedModules(this.mappedModules);
        tempMap.setMap(this.map);

        return tempMap;
    }

    /**
     * Setter method to set new map
     *
     * @param map
     *            the new map
     */
    public final void setMap(final Module[][] map) {
        for (int i = 0; i < MAXY; i++) {
            for (int j = 0; j < MAXX; j++) {
                this.map[i][j] = map[i][j];
            }
        }
    }

    /**
     * Setter to set new mapped module list
     *
     * @param mappedModules
     *            the list of mapped modules
     */
    public final void setMappedModules(final List<Module> mappedModules) {
        this.mappedModules = copyModules(mappedModules);
    }

    /**
     * Getter to get the current map
     *
     * @return the map
     */
    public final Module[][] getMap() {
        return this.map;
    }

    /**
     * Getter to get the current mapped module list
     *
     * @return the mapped module list
     */
    public final List<Module> getMappedModules() {
        return this.mappedModules;
    }

    /**
     * Set a new module onto the current position
     *
     * @param pos
     *            the current position
     * @param type
     *            the type of module
     * @return true if successful otherwise false
     */
    public final boolean setPosition(final List<Integer> pos, final Type type) {
        int y = pos.get(0);
        int x = pos.get(1);
        Module module;

        if ((y > 0 && y < MAXY) && (x > 0 && x < MAXX) && getNumberOfModules(type) >= 0 && !positionTaken(pos)) {
            module = getModuleFromData(type);

            if (module != null) {
                module.setXPosition(x);
                module.setYPosition(y);

                map[y][x] = module;
                mappedModules.add(module);
                return true;
            }
            return false;
        }

        return false;
    }

    /**
     * Check to see if there is any available modules left
     *
     * @return true if empty otherwise false
     */
    public final boolean noModuleLeft() {
        return modules.isEmpty();
    }

    /**
     * Check to see if the current position is taken by another module
     *
     * @param position
     *            the current position
     * @return true if the position is taken otherwise false
     */
    private boolean positionTaken(final List<Integer> position) {
        int[] pos = new int[3];
        int y = position.get(0);
        int x = position.get(1);

        for (Module m : mappedModules) {
            pos = m.getPosition();
            if (pos[0] == x && pos[1] == y) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get the type from map for the current position
     *
     * @param pos
     *            the current position
     * @return the module at current position otherwise null
     */
    public final Module getTypeFromPos(final List<Integer> pos) {
        int y = pos.get(0);
        int x = pos.get(1);
        if ((y > 0 && y < MAXY) && (x > 0 && x < MAXX)) {
            return map[y][x];
        }
        return null;
    }

    /**
     * Remove and get module from list of modules
     *
     * @param type
     *            the module type
     * @return return the module if found otherwise null
     */
    private Module getModuleFromData(final Type type) {
        for (int i = 0; i < modules.size(); i++) {
            if (modules.get(i).getType() == type) {
                return modules.remove(i);
            }
        }
        return null;
    }

    /**
     * Get the number of type left
     *
     * @param type
     *            the module type
     * @return the number of type left
     */
    public final int getNumberOfModules(final Type type) {
        int count = 0;
        for (Module mod : modules) {
            if (mod.getType() == type) {
                count++;
            }
        }

        return count;
    }

    /**
     * Check to see if the current module type is diagonal to the target module
     * type from the current position
     *
     * @param cur
     *            current module type
     * @param tar
     *            target module type
     * @param position
     *            current position
     * @return true if the target module type is diagonal to the current module
     *         type
     */
    @SuppressWarnings("unchecked")
    public final boolean isModuleDiagonalDiagonalTo(final Type cur, final Type tar, final List<Integer> position) {
        List<Integer> pos = (List<Integer>) copyList(position);
        int y = pos.get(0);
        int x = pos.get(1);

        pos.set(0, y + 1);
        pos.set(1, x + 1);
        if (getTypeFromPos(pos) != null && getTypeFromPos(pos).getType() == tar) {
            return thereExistDiagonalTo(cur, pos, 1);
        }

        pos.set(0, y - 1);
        pos.set(1, x - 1);
        if (getTypeFromPos(pos) != null && getTypeFromPos(pos).getType() == tar) {
            return thereExistDiagonalTo(cur, pos, 1);
        }

        pos.set(0, y + 1);
        pos.set(1, x - 1);
        if (getTypeFromPos(pos) != null && getTypeFromPos(pos).getType() == tar) {
            return thereExistDiagonalTo(cur, pos, 1);
        }

        pos.set(0, y - 1);
        pos.set(1, x + 1);
        if (getTypeFromPos(pos) != null && getTypeFromPos(pos).getType() == tar) {
            return thereExistDiagonalTo(cur, pos, 1);
        }

        return false;
    }

    /**
     * Method to obtain the list of position of the type
     *
     * @param type
     *            the module type
     * @return a list of the mapped module type
     */
    @SuppressWarnings("unchecked")
    private List<List<Integer>> listOfModulePositions(final Type type) {
        List<List<Integer>> listPos = new ArrayList<List<Integer>>();
        List<Integer> position = new ArrayList<Integer>();
        int[] pos = new int[3];
        int y;
        int x;

        for (Module module : mappedModules) {
            if (module.getType() == type) {
                pos = module.getPosition();
                y = pos[1];
                x = pos[0];

                position.clear();
                position.add(y);
                position.add(x);

                listPos.add((List<Integer>) copyList(position));
            }
        }

        return listPos;
    }

    /**
     * Check to see if all airlock have a medical diagonal to them
     *
     * @return true if all airlock have a medical diagonal to them, otherwise
     *         false
     */
    public final boolean medicAirlockFull() {
        List<List<Integer>> airlockPos = listOfModulePositions(Type.AIRLOCK);
        List<Integer> pos = new ArrayList<Integer>();

        while (!airlockPos.isEmpty()) {
            pos = airlockPos.remove(0);
            if (!thereExistDiagonalTo(Type.MEDICAL, pos, 1)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check to see if the is a module type of that same type is mapped
     *
     * @param type
     *            the current type
     * @return true if the type is mapped otherwise false
     */
    public final boolean thereExistMappedModule(final Type type) {
        for (Module module : modules) {
            if (module.getType() == type) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check to see if the target module type is diagonal from all side to from
     * the current position
     *
     * @param tar
     *            the target module type
     * @param pos
     *            the current position
     * @param numSpaces
     *            number of spaces
     * @return true if the target module is found otherwise false
     */
    public final boolean thereExistDiagonalTo(final Type tar, final List<Integer> pos, final int numSpaces) {
        return thereExistRDiagonalTo(tar, pos, numSpaces) || thereExistLDiagonalTo(tar, pos, numSpaces);
    }

    /**
     * Check to see if the target module type is diagonal from the left side to
     * from the current position
     *
     * @param tar
     *            the target module type
     * @param pos
     *            the current position
     * @param numSpaces
     *            number of spaces
     * @return true if the target module is found otherwise false
     */
    public final boolean thereExistLDiagonalTo(final Type tar, final List<Integer> pos, final int numSpaces) {
        return thereExistLDownDiagonalTo(tar, pos, numSpaces) || thereExistLUpDiagonalTo(tar, pos, numSpaces);
    }

    /**
     * Check to see if the target module type is diagonal from the left down
     * side to from the current position
     *
     * @param tar
     *            the target module type
     * @param pos
     *            the current position
     * @param numSpaces
     *            number of spaces
     * @return true if the target module is found otherwise false
     */
    public final boolean thereExistLDownDiagonalTo(Type tar, List<Integer> pos, final int numSpaces) {
        int y = pos.get(0);
        int x = pos.get(1);
        for (int i = 1; i <= numSpaces; i++) {
            if (map[y + i][x - i] != null && map[y + i][x - i].getType() == tar) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check to see if the target module type is diagonal from the left top side
     * to from the current position
     *
     * @param tar
     *            the target module type
     * @param pos
     *            the current position
     * @param numSpaces
     *            number of spaces
     * @return true if the target module is found otherwise false
     */
    public final boolean thereExistLUpDiagonalTo(final Type tar, final List<Integer> pos, final int numSpaces) {
        int y = pos.get(0);
        int x = pos.get(1);
        for (int i = 1; i <= numSpaces; i++) {
            if (map[y - i][x - i] != null && map[y - i][x - i].getType() == tar) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check to see if the target module type is diagonal from the right side to
     * from the current position
     *
     * @param tar
     *            the target module type
     * @param pos
     *            the current position
     * @param numSpaces
     *            number of spaces
     * @return true if the target module is found otherwise false
     */
    public final boolean thereExistRDiagonalTo(final Type tar, final List<Integer> pos, final int numSpaces) {
        return thereExistRUpDiagonalTo(tar, pos, numSpaces) || thereExistRDownDiagonalTo(tar, pos, numSpaces);
    }

    /**
     * Check to see if the target module type is diagonal from the right top
     * side to from the current position
     *
     * @param tar
     *            the target module type
     * @param pos
     *            the current position
     * @param numSpaces
     *            number of spaces
     * @return true if the target module is found otherwise false
     */
    public final boolean thereExistRUpDiagonalTo(final Type tar, final List<Integer> pos, final int numSpaces) {
        int y = pos.get(0);
        int x = pos.get(1);
        for (int i = 1; i <= numSpaces; i++) {
            if (map[y - i][x + i] != null && map[y - i][x + i].getType() == tar) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check to see if the target module type is diagonal from the right down
     * side to from the current position
     *
     * @param tar
     *            the target module type
     * @param pos
     *            the current position
     * @param numSpaces
     *            number of spaces
     * @return true if the target module is found otherwise false
     */
    public final boolean thereExistRDownDiagonalTo(final Type tar, final List<Integer> pos, final int numSpaces) {
        int y = pos.get(0);
        int x = pos.get(1);
        for (int i = 1; i <= numSpaces; i++) {
            if (map[y + i][x + i] != null && map[y + i][x + i].getType() == tar) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check to see if the target module type is horizontal from both side to
     * from the current position
     *
     * @param tar
     *            the target module type
     * @param pos
     *            the current position
     * @param numSpaces
     *            number of spaces
     * @return true if the target module is found otherwise false
     */
    public final boolean thereExistHorizontalTo(final Type tar, final List<Integer> pos, final int numSpaces) {
        return thereExistLHorizontalTo(tar, pos, numSpaces) || thereExistRHorizontalTo(tar, pos, numSpaces);
    }

    /**
     * Check to see if the target module type is horizontal from right side to
     * from the current position
     *
     * @param tar
     *            the target module type
     * @param pos
     *            the current position
     * @param numSpaces
     *            number of spaces
     * @return true if the target module is found otherwise false
     */
    public final boolean thereExistRHorizontalTo(final Type tar, final List<Integer> pos, final int numSpaces) {
        int y = pos.get(0);
        int x = pos.get(1);
        for (int i = 1; i <= numSpaces; i++) {
            if (x + i < MAXX && map[y][x + i] != null && map[y][x + i].getType() == tar) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check to see if the target module type is horizontal from left side to
     * from the current position
     *
     * @param tar
     *            the target module type
     * @param pos
     *            the current position
     * @param numSpaces
     *            number of spaces
     * @return true if the target module is found otherwise false
     */
    public final boolean thereExistLHorizontalTo(final Type tar, final List<Integer> pos, final int numSpaces) {
        int y = pos.get(0);
        int x = pos.get(1);
        for (int i = 1; i <= numSpaces; i++) {
            if (x - i > 0 && y < MAXY && map[y][x - i] != null && map[y][x - i].getType() == tar) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check to see if the target module type is vertical from both side to from
     * the current position
     *
     * @param tar
     *            the target module type
     * @param pos
     *            the current position
     * @param numSpaces
     *            number of spaces
     * @return true if the target module is found otherwise false
     */
    public final boolean thereExistVerticalTo(final Type tar, final List<Integer> pos, final int numSpaces) {
        return thereExistUpVerticalTo(tar, pos, numSpaces) || thereExistDownVerticalTo(tar, pos, numSpaces);
    }

    /**
     * Check to see if the target module type is vertical from top side to from
     * the current position
     *
     * @param tar
     *            the target module type
     * @param pos
     *            the current position
     * @param numSpaces
     *            number of spaces
     * @return true if the target module is found otherwise false
     */
    public final boolean thereExistUpVerticalTo(final Type tar, final List<Integer> pos, final int numSpaces) {
        int y = pos.get(0);
        int x = pos.get(1);
        for (int i = 1; i <= numSpaces; i++) {
            if (y - i > 0 && map[y - i][x] != null && map[y - i][x].getType() == tar) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check to see if the target module type is vertical from down side to from
     * the current position
     *
     * @param tar
     *            the target module type
     * @param pos
     *            the current position
     * @param numSpaces
     *            number of spaces
     * @return true if the target module is found otherwise false
     */
    public final boolean thereExistDownVerticalTo(final Type tar, final List<Integer> pos, final int numSpaces) {
        int y = pos.get(0);
        int x = pos.get(1);
        for (int i = 1; i <= numSpaces; i++) {
            if (map[y + i][x] != null && map[y + i][x].getType() == tar) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check to see if the target module type is vertical, horizontal and
     * diagonally from all side to from the current position
     *
     * @param tar
     *            the target module type
     * @param pos
     *            the current position
     * @param numSpaces
     *            number of spaces
     * @return true if the target module is found otherwise false
     */
    public final boolean thereExistAllSideTo(final Type tar, final List<Integer> pos, final int numSpaces) {
        if (thereExistVerticalTo(tar, pos, numSpaces)
                || thereExistDiagonalTo(tar, pos, numSpaces)
                || thereExistHorizontalTo(tar, pos, numSpaces)) {
            return true;
        }
        return false;
    }

    /**
     * tar is the list to be copied
     *
     * @param tar
     *            the list to be copied
     * @return copied list of tar
     */
    private List<?> copyList(final List<?> tar) {
        List<Object> copyList = new ArrayList<Object>();

        for (int i = 0; i < tar.size(); i++) {
            copyList.add(tar.get(i));
        }

        return copyList;
    }

    /**
     * Copy list of module
     *
     * @param tar
     *            the list to be copied
     * @return a copied list of tar
     */
    private List<Module> copyModules(final List<Module> tar) {
        List<Module> copyList = new ArrayList<Module>();

        for (Module module : tar) {
            copyList.add(module.clone());
        }

        return copyList;
    }
}
