package backend.calculate.path;

import java.util.ArrayList;
import java.util.List;

public class Path {
    List<List<Integer>> path;

    public Path() {
        path = new ArrayList<List<Integer>>();
    }

    public List<List<Integer>> getPath() {
        return path;
    }

    public void addPath(List<Integer> newPos) {
        path.add(copyList(newPos));
    }

    public void setPath(List<List<Integer>> path) {
        this.path = new ArrayList<List<Integer>>();
        for (List<Integer> i : path) {
            this.path.add(copyList(i));
        }
    }

    public void clear() {
        this.path = new ArrayList<List<Integer>>();
    }

    public boolean removePrevous() {
        if (!path.isEmpty()) {
            path.remove(path.size() - 1);
            return true;
        }
        return false;
    }

    private List<Integer> copyList(List<Integer> list) {
        List<Integer> copyList = new ArrayList<Integer>();

        for (Integer i : list) {
            copyList.add(i);
        }
        return copyList;
    }
}
