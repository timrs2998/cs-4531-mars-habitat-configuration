package backend.calculate.path;

import backend.common.Module;
import java.util.ArrayList;
import java.util.List;

public class Rover {
    private List<List<Integer>> moduleLocation;
    private List<Integer> currentPos;
    private boolean hold;
    private Module holdModule;
    private Path path;

    public Rover(List<Integer> curPos, List<Module> original) {
        this.currentPos = copyList(curPos);
        this.hold = false;
        this.holdModule = null;
        moduleLocation = new ArrayList<List<Integer>>();

        path = new Path() {};
        path.addPath(curPos);

        int[] pos;
        List<Integer> position = new ArrayList<Integer>();
        for (Module m : original) {
            pos = m.getPosition();

            position.add(pos[1]);
            position.add(pos[0]);

            moduleLocation.add(copyList(position));
        }
    }

    public List<Integer> getCurrentPosition() {
        return this.currentPos;
    }

    public List<List<Integer>> getPathTaken() {
        return path.getPath();
    }

    public Module getHoldingModule() {
        return holdModule;
    }

    public boolean isHolding() {
        return hold;
    }

    public void holdModule(Module module) {
        if (!hold
                && moduleLocation.contains(currentPos)
                && currentPos.get(0).equals(module.getPosition()[1])
                && currentPos.get(1).equals(module.getPosition()[0])) {
            moduleLocation.remove(currentPos);
            holdModule = module.clone();
            hold = true;
        }
    }

    public boolean placeModule() {
        if (hold && !moduleLocation.contains(currentPos)) {
            moduleLocation.add(currentPos);
            this.holdModule = null;
            this.hold = false;

            return true;
        }
        return false;
    }

    public void moveTo(List<Integer> tarPos) {
        if (!this.currentPos.equals(tarPos)) {
            currentPos = copyList(tarPos);
            path.addPath(currentPos);
        }
    }

    public void calculatePathTo(List<Integer> tarPos) {
        List<List<Integer>> path = calculatePath(currentPos, tarPos);

        for (List<Integer> pos : path) {
            moveTo(pos);
        }
    }

    private List<List<Integer>> calculatePath(List<Integer> curPos, List<Integer> tarPos) {
        List<List<Integer>> path = new ArrayList<List<Integer>>();
        List<Integer> nextPos = copyList(curPos);
        int tarY = tarPos.get(0);
        int tarX = tarPos.get(1);
        int y;
        int x;

        while (!nextPos.equals(tarPos)) {
            y = nextPos.get(0);
            x = nextPos.get(1);

            if (x < tarX) {
                nextPos.set(1, x + 1);
            } else if (x > tarX) {
                nextPos.set(1, x - 1);
            } else if (y < tarY) {
                nextPos.set(0, y + 1);
            } else if (y > tarY) {
                nextPos.set(0, y - 1);
            }

            path.add(copyList(nextPos));
        }

        return path;
    }

    private List<Integer> copyList(List<Integer> list) {
        List<Integer> copyList = new ArrayList<Integer>();

        for (Integer i : list) {
            copyList.add(i);
        }

        return copyList;
    }
}
