package backend.calculate.path;

import backend.common.Module;
import java.util.ArrayList;
import java.util.List;

public class Calculation {
    List<Module> mapped;
    List<Module> original;

    Rover rover;

    public Calculation(List<Module> mapped, List<Module> original, List<Integer> roverPosition) {
        this.mapped = copyModuleList(mapped);
        this.original = copyModuleList(original);
        this.rover = new Rover(roverPosition, original);
    }

    public List<List<Integer>> calculatePath() {
        List<Integer> position = new ArrayList<Integer>();
        int[] pos = new int[3];
        int y;
        int x;

        position.add(null);
        position.add(null);

        for (Module mappedModule : mapped) {
            for (Module module : original) {
                if (mappedModule.getCode() == module.getCode()) {
                    pos = module.getPosition();
                    x = pos[0];
                    y = pos[1];

                    position.set(0, y);
                    position.set(1, x);

                    rover.calculatePathTo(position);
                    rover.holdModule(module);

                    pos = mappedModule.getPosition();
                    x = pos[0];
                    y = pos[1];

                    position.set(0, y);
                    position.set(1, x);

                    rover.calculatePathTo(position);
                    rover.placeModule();
                    break;
                }
            }
        }
        return rover.getPathTaken();
    }

    private List<Module> copyModuleList(List<Module> tar) {
        List<Module> copyList = new ArrayList<Module>();

        for (Module m : tar) {
            copyList.add(m.clone());
        }

        return copyList;
    }
}
