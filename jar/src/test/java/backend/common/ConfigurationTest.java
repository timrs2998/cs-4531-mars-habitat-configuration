package backend.common;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import backend.common.Configuration.Rule;
import java.util.LinkedList;
import java.util.List;
import org.junit.jupiter.api.Test;

public class ConfigurationTest {

    @Test
    public final void testRule1() {
        final Rule rule = new Configuration.Rule1();

        // Test that the rule is violated (next to horizontally)
        final List<Module> modules = new LinkedList<Module>();
        modules.add(new Module(91, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(141, new int[] {1, 0, 0}, Status.UNDAMAGED, 0));
        assertFalse(rule.isFollowed(modules));

        // Test that the rule is violated (next to vertical + diagonally))
        modules.clear();
        modules.add(new Module(91, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(141, new int[] {1, 1, 1}, Status.UNDAMAGED, 0));
        assertFalse(rule.isFollowed(modules));

        // Test that rule is not violated.
        modules.clear();
        modules.add(new Module(91, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(141, new int[] {2, 0, 0}, Status.UNDAMAGED, 0));
        assertTrue(rule.isFollowed(modules));
    }

    @Test
    public final void testRule2() {
        final Rule rule = new Configuration.Rule2();

        // Test that the rule is violated
        final List<Module> modules = new LinkedList<Module>();
        modules.add(new Module(91, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(111, new int[] {1, 0, 0}, Status.UNDAMAGED, 0));
        assertFalse(rule.isFollowed(modules));

        // Test that rule is not violated.
        modules.clear();
        modules.add(new Module(91, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(111, new int[] {2, 0, 0}, Status.UNDAMAGED, 0));
        assertTrue(rule.isFollowed(modules));
    }

    @Test
    public final void testRule3() {
        final Rule rule = new Configuration.Rule3();

        // Test that the rule is violated
        final List<Module> modules = new LinkedList<Module>();
        modules.add(new Module(171, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(61, new int[] {1, 0, 0}, Status.UNDAMAGED, 0));
        assertFalse(rule.isFollowed(modules));

        // Test that rule is not violated.
        modules.clear();
        modules.add(new Module(171, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(61, new int[] {2, 0, 0}, Status.UNDAMAGED, 0));
        assertTrue(rule.isFollowed(modules));
    }

    @Test
    public final void testRule5() {
        final Rule rule = new Configuration.Rule5();

        // Test that the rule is violated
        final List<Module> modules = new LinkedList<Module>();
        modules.add(new Module(1, new int[] {1, 1, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(2, new int[] {1, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(3, new int[] {1, 2, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(4, new int[] {0, 1, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(5, new int[] {2, 1, 0}, Status.UNDAMAGED, 0));
        assertFalse(rule.isFollowed(modules));

        // Test that rule is not violated.
        modules.clear();
        modules.add(new Module(1, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(2, new int[] {1, 0, 0}, Status.UNDAMAGED, 0));
        assertTrue(rule.isFollowed(modules));
    }

    @Test
    public final void testRule8() {
        final Rule rule = new Configuration.Rule8();

        // Test that the rule is violated (1 GymRelax, 0 Sanitation)
        final List<Module> modules = new LinkedList<Module>();
        modules.add(new Module(131, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        assertFalse(rule.isFollowed(modules));

        // Test that the rule is violated (4 GymRelax, 4 Sanitation)
        modules.clear();
        modules.add(new Module(131, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(131, new int[] {1, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(131, new int[] {2, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(131, new int[] {3, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(91, new int[] {0, 1, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(91, new int[] {1, 1, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(91, new int[] {2, 1, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(91, new int[] {3, 4, 0}, Status.UNDAMAGED, 0));
        assertFalse(rule.isFollowed(modules));

        // Test that rule is not violated.
        modules.clear();
        modules.add(new Module(131, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(131, new int[] {1, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(131, new int[] {2, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(131, new int[] {3, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(91, new int[] {0, 1, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(91, new int[] {1, 1, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(91, new int[] {2, 1, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(91, new int[] {3, 1, 0}, Status.UNDAMAGED, 0));
        assertTrue(rule.isFollowed(modules));
    }

    // @Test
    // public final void testRule9() {
    // final Rule rule = new Configuration.Rule9();
    //
    // // Test that the rule is violated
    // final List<Module> modules = new LinkedList<Module>();
    // modules.add(new Module(181, new int[] { 0, 0, 0 }, Status.UNDAMAGED, 0));
    // assertFalse(rule.isFollowed(modules));
    //
    // // Test that the rule is violated
    // modules.clear();
    // modules.add(new Module(181, new int[] { 0, 0, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(181, new int[] { 1, 0, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(181, new int[] { 2, 0, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(181, new int[] { 3, 0, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(181, new int[] { 4, 0, 0 }, Status.UNDAMAGED, 0));
    //
    // modules.add(new Module(171, new int[] { 1, 1, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(171, new int[] { 2, 1, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(171, new int[] { 3, 1, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(171, new int[] { 4, 1, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(171, new int[] { 5, 1, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(171, new int[] { 7, 1, 0 }, Status.UNDAMAGED, 0));
    // assertFalse(rule.isFollowed(modules));
    //
    // // Test that the rule is not violated
    // modules.clear();
    // modules.add(new Module(181, new int[] { 0, 0, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(181, new int[] { 1, 0, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(181, new int[] { 2, 0, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(181, new int[] { 3, 0, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(181, new int[] { 4, 0, 0 }, Status.UNDAMAGED, 0));
    //
    // modules.add(new Module(171, new int[] { 1, 1, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(171, new int[] { 2, 1, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(171, new int[] { 3, 1, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(171, new int[] { 4, 1, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(171, new int[] { 5, 1, 0 }, Status.UNDAMAGED, 0));
    // assertTrue(rule.isFollowed(modules));
    //
    // // Test that rule is not violated.
    // modules.clear();
    // modules.add(new Module(181, new int[] { 0, 0, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(171, new int[] { 1, 1, 0 }, Status.UNDAMAGED, 0));
    // assertTrue(rule.isFollowed(modules));
    // }

    @Test
    public final void testRuleAirlockPlain() {
        final Rule rule = new Configuration.RuleAirlockPlain();

        // Test that the rule is violated
        final List<Module> modules = new LinkedList<Module>();
        modules.add(new Module(171, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        assertFalse(rule.isFollowed(modules));

        // Test that rule is not violated.
        modules.clear();
        modules.add(new Module(171, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(1, new int[] {1, 0, 0}, Status.UNDAMAGED, 0));
        assertTrue(rule.isFollowed(modules));
    }

    //
    // @Test
    // public final void testRule12() {
    // final Rule rule = new Configuration.Rule12();
    //
    // // Test that the rule is violated
    // final List<Module> modules = new LinkedList<Module>();
    // modules.add(new Module(171, new int[] { 0, 0, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(171, new int[] { 1, 0, 0 }, Status.UNDAMAGED, 0));
    // assertFalse(rule.isFollowed(modules));
    //
    // // Test that rule is not violated.
    // modules.clear();
    // modules.add(new Module(171, new int[] { 0, 0, 0 }, Status.UNDAMAGED, 0));
    // modules.add(new Module(171, new int[] { 2, 0, 0 }, Status.UNDAMAGED, 0));
    // assertTrue(rule.isFollowed(modules));
    // }

    @Test
    public final void testIsConnected() {
        final Rule rule = new Configuration.IsConnected();

        // Test that the rule is violated
        final List<Module> modules = new LinkedList<Module>();
        modules.add(new Module(0, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(0, new int[] {2, 0, 0}, Status.UNDAMAGED, 0));
        assertFalse(rule.isFollowed(modules));

        // Test that rule is not violated.
        modules.clear();
        modules.add(new Module(0, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(0, new int[] {1, 0, 0}, Status.UNDAMAGED, 0));
        assertTrue(rule.isFollowed(modules));
    }

    @Test
    public final void testNoDuplicates() {
        final Rule rule = new Configuration.NoDuplicates();

        // Test that the rule is violated
        final List<Module> modules = new LinkedList<Module>();
        modules.add(new Module(0, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(0, new int[] {2, 0, 0}, Status.UNDAMAGED, 0));
        assertFalse(rule.isFollowed(modules));

        // Test that rule is not violated.
        modules.clear();
        modules.add(new Module(0, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(1, new int[] {1, 0, 0}, Status.UNDAMAGED, 0));
        assertTrue(rule.isFollowed(modules));
    }

    @Test
    public final void testNoOverlaps() {
        final Rule rule = new Configuration.NoDuplicates();

        // Test that the rule is violated
        final List<Module> modules = new LinkedList<Module>();
        modules.add(new Module(0, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(0, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        assertFalse(rule.isFollowed(modules));

        // Test that rule is not violated.
        modules.clear();
        modules.add(new Module(0, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        modules.add(new Module(1, new int[] {1, 0, 0}, Status.UNDAMAGED, 0));
        assertTrue(rule.isFollowed(modules));
    }

    @Test
    public final void testEquals() {
        // Test for inequality
        final List<Module> modules = new LinkedList<Module>();
        modules.add(new Module(0, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        final Configuration c = new Configuration(modules);
        assertFalse(c.equals(null));

        // Test for inequality
        final List<Module> modules2 = new LinkedList<Module>();
        modules2.add(new Module(0, new int[] {0, 1, 0}, Status.UNDAMAGED, 0));
        Configuration c2 = new Configuration(modules2);
        assertFalse(c.equals(c2));

        // Test for equality
        modules2.clear();
        modules2.add(new Module(0, new int[] {0, 0, 0}, Status.UNDAMAGED, 0));
        c2 = new Configuration(modules2);
        assertTrue(c.equals(c2));
    }
}
