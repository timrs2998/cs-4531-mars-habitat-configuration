package backend.common;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class ModuleTest {

    @Test
    public final void testIsNextTo() {
        Module mod1 = new Module(1, new int[] {0, 0, 0}, Status.UNDAMAGED, 0);
        Module mod2 = new Module(1, new int[] {1, 1, 1}, Status.UNDAMAGED, 0);
        assertTrue(mod1.isNextTo(mod2));

        mod1 = new Module(1, new int[] {0, 0, 0}, Status.UNDAMAGED, 0);
        mod2 = new Module(1, new int[] {2, 1, 1}, Status.UNDAMAGED, 0);
        assertFalse(mod1.isNextTo(mod2));
    }

    @Test
    public final void testIsNextToHorizontal() {
        // Test not next to
        Module mod1 = new Module(1, new int[] {0, 0, 0}, Status.UNDAMAGED, 0);
        Module mod2 = new Module(1, new int[] {1, 1, 0}, Status.UNDAMAGED, 0);
        assertFalse(mod1.isNextToHorizontal(mod2));

        mod1 = new Module(1, new int[] {0, 0, 0}, Status.UNDAMAGED, 0);
        mod2 = new Module(1, new int[] {1, 1, 1}, Status.UNDAMAGED, 0);
        assertFalse(mod1.isNextToHorizontal(mod2));

        mod1 = new Module(1, new int[] {0, 0, 0}, Status.UNDAMAGED, 0);
        mod2 = new Module(1, new int[] {2, 1, 1}, Status.UNDAMAGED, 0);
        assertFalse(mod1.isNextToHorizontal(mod2));

        // Test next to
        mod1 = new Module(1, new int[] {0, 0, 0}, Status.UNDAMAGED, 0);
        mod2 = new Module(1, new int[] {1, 0, 0}, Status.UNDAMAGED, 0);
        assertTrue(mod1.isNextToHorizontal(mod2));
    }

    @Test
    public final void testEquals() {
        // Test for inequality
        Module mod1 = new Module(1, new int[] {0, 0, 0}, Status.UNDAMAGED, 0);
        Module mod2 = new Module(1, new int[] {1, 1, 0}, Status.UNDAMAGED, 0);
        assertFalse(mod1.equals(mod2));

        // Test for equality
        mod1 = new Module(1, new int[] {0, 0, 0}, Status.UNDAMAGED, 0);
        mod2 = new Module(1, new int[] {0, 0, 0}, Status.UNDAMAGED, 0);
        assertTrue(mod1.equals(mod2));
    }

    @Test
    public final void testClone() {
        // Test for equality
        Module mod1 = new Module(1, new int[] {0, 0, 0}, Status.UNDAMAGED, 0);
        Module mod2 = mod1.clone();
        assertTrue(mod1.equals(mod2));

        // Check they aren't the same objects
        assertFalse(mod1 == mod2);
        assertFalse(mod1.getPosition() == mod2.getPosition());
    }
}
