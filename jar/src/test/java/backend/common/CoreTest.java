package backend.common;

import static org.junit.jupiter.api.Assertions.assertFalse;

public class CoreTest {

    //    @Test
    public void testGetModule() {
        final Core core = new Core();
        core.addModule(new Module(1, new int[] {1, 2, 3}, Status.UNDAMAGED, 0));
        assertFalse(core.getModules().isEmpty());
    }
}
